#include "MapperTestUtils.hpp"

#include "Mappers/Mapper1.hpp"


class Mapper1Test : public ::testing::Test {
public:
	enum Registers {
		Control,
		CharacterLo,
		CharacterHi,
		Program
	};

	Mapper1Test(uint8 programROMChunks, bool noCharacterROMChunks) : mapper(std::make_unique<nes::Mapper1>(programROMChunks, noCharacterROMChunks)) {}

	void WriteRegister(Registers reg, uint8 value) {
		const uint16 address = 0x8000 | (static_cast<uint16>(reg) << 13);
		for (uint8 i = 0; i < 5; ++i) {
			nes::Mapper::Result result = mapper->MapCPUAddressForWrite(address, (value >> i) & 1);
			EXPECT_THAT(result, IsEmptyResult());
		}
	}

protected:
	std::unique_ptr<nes::Mapper1> mapper;
};


class Mapper1WithChrROMTest : public Mapper1Test {
public:
	Mapper1WithChrROMTest() : Mapper1Test(10, false) {}
};


class Mapper1WithoutChrROMTest : public Mapper1Test {
public:
	Mapper1WithoutChrROMTest() : Mapper1Test(10, true) {}
};


TEST_F(Mapper1WithChrROMTest, RAMTest) {
	nes::Mapper::Result result;
	result = mapper->MapCPUAddressForRead(0x6000);
	EXPECT_THAT(result, IsDataFromRAM(0x00));

	result = mapper->MapCPUAddressForRead(0x789A);
	EXPECT_THAT(result, IsDataFromRAM(0x00));

	result = mapper->MapCPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsDataFromRAM(0x00));

	result = mapper->MapCPUAddressForWrite(0x6000, 0x12);
	EXPECT_THAT(result, IsDataFromRAM(0x12));

	result = mapper->MapCPUAddressForRead(0x6000);
	EXPECT_THAT(result, IsDataFromRAM(0x12));

	result = mapper->MapCPUAddressForWrite(0x789A, 0x34);
	EXPECT_THAT(result, IsDataFromRAM(0x34));

	result = mapper->MapCPUAddressForRead(0x789A);
	EXPECT_THAT(result, IsDataFromRAM(0x34));

	result = mapper->MapCPUAddressForWrite(0x7FFF, 0x56);
	EXPECT_THAT(result, IsDataFromRAM(0x56));

	result = mapper->MapCPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsDataFromRAM(0x56));
}


TEST_F(Mapper1WithChrROMTest, CPURead32KMode) {
	WriteRegister(Control, 0x00);

	nes::Mapper::Result result;
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x1876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x7FFF));

	WriteRegister(Program, 0x02);
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x8000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x9876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0xFFFF));

	WriteRegister(Program, 0x05);
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x10000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x11876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x17FFF));

	WriteRegister(Program, 0x1F);
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x38000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x39876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFFF));
}


TEST_F(Mapper1WithChrROMTest, CPURead16KModeHiFixedReadFromLo) {
	WriteRegister(Control, 0x0C);

	nes::Mapper::Result result;
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x1876));

	result = mapper->MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));

	WriteRegister(Program, 0x01);
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x4000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x5876));

	result = mapper->MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x7FFF));

	WriteRegister(Program, 0x06);
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x18000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x19876));

	result = mapper->MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x1BFFF));

	WriteRegister(Program, 0x1F);
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x3C000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x3D876));

	result = mapper->MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFFF));
}


TEST_F(Mapper1WithChrROMTest, CPURead16KModeLoFixedReadFromHi) {
	WriteRegister(Control, 0x08);

	nes::Mapper::Result result;
	result = mapper->MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapCPUAddressForRead(0xD876);
	EXPECT_THAT(result, IsMappedAddress(0x1876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));

	WriteRegister(Program, 0x01);
	result = mapper->MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x4000));

	result = mapper->MapCPUAddressForRead(0xD876);
	EXPECT_THAT(result, IsMappedAddress(0x5876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x7FFF));

	WriteRegister(Program, 0x06);
	result = mapper->MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x18000));

	result = mapper->MapCPUAddressForRead(0xD876);
	EXPECT_THAT(result, IsMappedAddress(0x19876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x1BFFF));

	WriteRegister(Program, 0x1F);
	result = mapper->MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x3C000));

	result = mapper->MapCPUAddressForRead(0xD876);
	EXPECT_THAT(result, IsMappedAddress(0x3D876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFFF));
}


TEST_F(Mapper1WithChrROMTest, CPURead16KModeLoFixedReadFromLo) {
	WriteRegister(Control, 0x08);

	nes::Mapper::Result result;
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x24000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x25876));

	result = mapper->MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x27FFF));

	WriteRegister(Program, 0x01);
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x24000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x25876));

	result = mapper->MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x27FFF));

	WriteRegister(Program, 0x06);
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x24000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x25876));

	result = mapper->MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x27FFF));

	WriteRegister(Program, 0x1F);
	result = mapper->MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x24000));

	result = mapper->MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x25876));

	result = mapper->MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x27FFF));
}


TEST_F(Mapper1WithChrROMTest, CPURead16KModeHiFixedReadFromHi) {
	WriteRegister(Control, 0x0C);

	nes::Mapper::Result result;
	result = mapper->MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x24000));

	result = mapper->MapCPUAddressForRead(0xD876);
	EXPECT_THAT(result, IsMappedAddress(0x25876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x27FFF));

	WriteRegister(Program, 0x01);
	result = mapper->MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x24000));

	result = mapper->MapCPUAddressForRead(0xD876);
	EXPECT_THAT(result, IsMappedAddress(0x25876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x27FFF));

	WriteRegister(Program, 0x06);
	result = mapper->MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x24000));

	result = mapper->MapCPUAddressForRead(0xD876);
	EXPECT_THAT(result, IsMappedAddress(0x25876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x27FFF));

	WriteRegister(Program, 0x1F);
	result = mapper->MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x24000));

	result = mapper->MapCPUAddressForRead(0xD876);
	EXPECT_THAT(result, IsMappedAddress(0x25876));

	result = mapper->MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x27FFF));
}


TEST_F(Mapper1WithChrROMTest, CPUReadOutOfRange) {
	WriteRegister(Control, 0x0C);

	nes::Mapper::Result result;
	result = mapper->MapCPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapCPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapCPUAddressForRead(0x5FFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST_F(Mapper1WithChrROMTest, PPURead8KMode) {
	WriteRegister(Control, 0x00);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterLo, 0x02);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x2000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x3234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));

	WriteRegister(CharacterLo, 0x05);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x4000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x5234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x5FFF));

	WriteRegister(CharacterLo, 0x1F);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x1E000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1F234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFFF));
}


TEST_F(Mapper1WithChrROMTest, PPURead4KModeReadFromLo) {
	WriteRegister(Control, 0x10);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForRead(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));

	WriteRegister(CharacterLo, 0x01);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x1000));

	result = mapper->MapPPUAddressForRead(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));
	
	result = mapper->MapPPUAddressForRead(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterLo, 0x06);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x6000));

	result = mapper->MapPPUAddressForRead(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x6234));

	result = mapper->MapPPUAddressForRead(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x6FFF));

	WriteRegister(CharacterLo, 0x1F);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x1F000));

	result = mapper->MapPPUAddressForRead(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x1F234));

	result = mapper->MapPPUAddressForRead(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFFF));
}


TEST_F(Mapper1WithChrROMTest, PPURead4KModeReadFromHi) {
	WriteRegister(Control, 0x10);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForRead(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));

	WriteRegister(CharacterHi, 0x01);
	result = mapper->MapPPUAddressForRead(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x1000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterHi, 0x06);
	result = mapper->MapPPUAddressForRead(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x6000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x6234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x6FFF));

	WriteRegister(CharacterHi, 0x1F);
	result = mapper->MapPPUAddressForRead(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x1F000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1F234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFFF));
}


TEST_F(Mapper1WithChrROMTest, PPUReadCharacterHiNotSetWhenIn8KMode) {
	WriteRegister(Control, 0x10);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForRead(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));

	WriteRegister(Control, 0x00);
	WriteRegister(CharacterHi, 0x1F);
	WriteRegister(Control, 0x10);

	result = mapper->MapPPUAddressForRead(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));
}


TEST_F(Mapper1WithChrROMTest, PPUReadOutOfRange) {
	WriteRegister(Control, 0x10);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForRead(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());

	WriteRegister(Control, 0x00);
	result = mapper->MapPPUAddressForRead(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST_F(Mapper1WithChrROMTest, PPUWrite) {
	WriteRegister(Control, 0x10);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForWrite(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());

	WriteRegister(Control, 0x00);
	result = mapper->MapPPUAddressForWrite(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST_F(Mapper1WithChrROMTest, GetMirroring) {
	WriteRegister(Control, 0x00);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::ONESCREEN_LO);

	WriteRegister(Control, 0x14);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::ONESCREEN_LO);

	WriteRegister(Control, 0x01);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::ONESCREEN_HI);

	WriteRegister(Control, 0x15);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::ONESCREEN_HI);

	WriteRegister(Control, 0x02);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::VERTICAL);

	WriteRegister(Control, 0x16);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::VERTICAL);

	WriteRegister(Control, 0x03);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::HORIZONTAL);

	WriteRegister(Control, 0x17);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::HORIZONTAL);
}


TEST_F(Mapper1WithoutChrROMTest, PPURead4KMode) {
	WriteRegister(Control, 0x10);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForRead(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));

	result = mapper->MapPPUAddressForRead(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x1000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterLo, 0x1F);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForRead(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));

	result = mapper->MapPPUAddressForRead(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x1000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterHi, 0x1F);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForRead(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));

	result = mapper->MapPPUAddressForRead(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x1000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));
}


TEST_F(Mapper1WithoutChrROMTest, PPURead8KMode) {
	WriteRegister(Control, 0x00);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterLo, 0x1F);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterHi, 0x1F);
	result = mapper->MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));
}


TEST_F(Mapper1WithoutChrROMTest, PPUReadOutOfRange) {
	WriteRegister(Control, 0x10);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForRead(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());

	WriteRegister(Control, 0x00);
	result = mapper->MapPPUAddressForRead(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST_F(Mapper1WithoutChrROMTest, PPUWrite4KMode) {
	WriteRegister(Control, 0x10);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForWrite(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForWrite(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForWrite(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));

	result = mapper->MapPPUAddressForWrite(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x1000));

	result = mapper->MapPPUAddressForWrite(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForWrite(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterLo, 0x1F);
	result = mapper->MapPPUAddressForWrite(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForWrite(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForWrite(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));

	result = mapper->MapPPUAddressForWrite(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x1000));

	result = mapper->MapPPUAddressForWrite(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForWrite(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterHi, 0x1F);
	result = mapper->MapPPUAddressForWrite(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForWrite(0x0234);
	EXPECT_THAT(result, IsMappedAddress(0x0234));

	result = mapper->MapPPUAddressForWrite(0x0FFF);
	EXPECT_THAT(result, IsMappedAddress(0x0FFF));

	result = mapper->MapPPUAddressForWrite(0x1000);
	EXPECT_THAT(result, IsMappedAddress(0x1000));

	result = mapper->MapPPUAddressForWrite(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForWrite(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));
}


TEST_F(Mapper1WithoutChrROMTest, PPUWrite8KMode) {
	WriteRegister(Control, 0x00);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForWrite(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForWrite(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForWrite(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterLo, 0x1F);
	result = mapper->MapPPUAddressForWrite(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForWrite(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForWrite(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	WriteRegister(CharacterHi, 0x1F);
	result = mapper->MapPPUAddressForWrite(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper->MapPPUAddressForWrite(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper->MapPPUAddressForWrite(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));
}


TEST_F(Mapper1WithoutChrROMTest, PPUWriteOutOfRange) {
	WriteRegister(Control, 0x10);

	nes::Mapper::Result result;
	result = mapper->MapPPUAddressForWrite(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());

	WriteRegister(Control, 0x00);
	result = mapper->MapPPUAddressForWrite(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper->MapPPUAddressForWrite(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST_F(Mapper1WithoutChrROMTest, GetMirroring) {
	WriteRegister(Control, 0x00);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::ONESCREEN_LO);

	WriteRegister(Control, 0x14);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::ONESCREEN_LO);

	WriteRegister(Control, 0x01);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::ONESCREEN_HI);

	WriteRegister(Control, 0x15);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::ONESCREEN_HI);

	WriteRegister(Control, 0x02);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::VERTICAL);

	WriteRegister(Control, 0x16);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::VERTICAL);

	WriteRegister(Control, 0x03);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::HORIZONTAL);

	WriteRegister(Control, 0x17);
	EXPECT_EQ(mapper->GetMirroring(), nes::MirroringMode::HORIZONTAL);
}
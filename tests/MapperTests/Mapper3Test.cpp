#include "MapperTestUtils.hpp"

#include "Mappers/Mapper3.hpp"


TEST(Mapper3Test, SingleBankCPURead) {
	nes::Mapper3 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0x8123);
	EXPECT_THAT(result, IsMappedAddress(0x0123));

	result = mapper.MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x1876));

	result = mapper.MapCPUAddressForRead(0xABCD);
	EXPECT_THAT(result, IsMappedAddress(0x2BCD));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));

	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0xD001);
	EXPECT_THAT(result, IsMappedAddress(0x1001));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));

	result = mapper.MapCPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST(Mapper3Test, DualBankCPURead) {
	nes::Mapper3 mapper(2);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0x8123);
	EXPECT_THAT(result, IsMappedAddress(0x0123));

	result = mapper.MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x1876));

	result = mapper.MapCPUAddressForRead(0xABCD);
	EXPECT_THAT(result, IsMappedAddress(0x2BCD));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));

	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x4000));

	result = mapper.MapCPUAddressForRead(0xD001);
	EXPECT_THAT(result, IsMappedAddress(0x5001));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x7FFF));

	result = mapper.MapCPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST(Mapper3Test, PPUReadFirstBank) {
	nes::Mapper3 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));
}


TEST(Mapper3Test, PPUReadSecondBank) {
	nes::Mapper3 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0x01);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x2000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x3234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));
}


TEST(Mapper3Test, PPUReadThirdBankWithHiBitsSet) {
	nes::Mapper3 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0xFE);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x4000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x5234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x5FFF));
}


TEST(Mapper3Test, PPUReadMaxBank) {
	nes::Mapper3 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x6000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x7234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x7FFF));
}


TEST(Mapper3Test, PPUReadInvalidSet) {
	nes::Mapper3 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x7FFF, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));
}


TEST(Mapper3Test, PPUReadOutOfRange) {
	nes::Mapper3 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapPPUAddressForRead(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());
}
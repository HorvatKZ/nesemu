#include "MapperTestUtils.hpp"

#include "Mappers/Mapper2.hpp"


TEST(Mapper2Test, CPUReadOutOfRange) {
	nes::Mapper2 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST(Mapper2Test, SettableBankCPUReadFirstBank) {
	nes::Mapper2 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x1ABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));
}


TEST(Mapper2Test, SettableBankCPUReadSecondBank) {
	nes::Mapper2 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0x01);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x4000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x5ABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x7FFF));
}


TEST(Mapper2Test, SettableBankCPUReadThirdBankWithHiBitsSet) {
	nes::Mapper2 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0xF2);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x8000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x9ABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0xBFFF));
}


TEST(Mapper2Test, SettableBankCPUReadMaxBank) {
	nes::Mapper2 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x3C000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x3DABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFFF));
}


TEST(Mapper2Test, SettableBankCPUReadInvalidSet) {
	nes::Mapper2 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x7FFF, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x1ABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));
}


TEST(Mapper2Test, FixedBankCPURead) {
	nes::Mapper2 mapper(16);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x3C000));

	result = mapper.MapCPUAddressForRead(0xDABC);
	EXPECT_THAT(result, IsMappedAddress(0x3DABC));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFFF));

	result = mapper.MapCPUAddressForWrite(0x8000, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x3C000));

	result = mapper.MapCPUAddressForRead(0xDABC);
	EXPECT_THAT(result, IsMappedAddress(0x3DABC));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFFF));
}


TEST(Mapper2Test, PPURead) {
	nes::Mapper2 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	result = mapper.MapPPUAddressForRead(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());
}
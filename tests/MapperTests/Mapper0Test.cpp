#include "MapperTestUtils.hpp"

#include "Mappers/Mapper0.hpp"


TEST(Mapper0Test, SingleBankCPURead) {
	nes::Mapper0 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0x8123);
	EXPECT_THAT(result, IsMappedAddress(0x0123));

	result = mapper.MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x1876));

	result = mapper.MapCPUAddressForRead(0xABCD);
	EXPECT_THAT(result, IsMappedAddress(0x2BCD));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));

	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0xD001);
	EXPECT_THAT(result, IsMappedAddress(0x1001));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));
	
	result = mapper.MapCPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsEmptyResult());
	
	result = mapper.MapCPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST(Mapper0Test, DualBankCPURead) {
	nes::Mapper0 mapper(2);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0x8123);
	EXPECT_THAT(result, IsMappedAddress(0x0123));

	result = mapper.MapCPUAddressForRead(0x9876);
	EXPECT_THAT(result, IsMappedAddress(0x1876));

	result = mapper.MapCPUAddressForRead(0xABCD);
	EXPECT_THAT(result, IsMappedAddress(0x2BCD));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));

	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x4000));

	result = mapper.MapCPUAddressForRead(0xD001);
	EXPECT_THAT(result, IsMappedAddress(0x5001));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x7FFF));
	
	result = mapper.MapCPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsEmptyResult());
	
	result = mapper.MapCPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST(Mapper0Test, SingleBankCPUWrite) {
	nes::Mapper0 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0x00);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0x8123, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0x9876, 0xAB);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xABCD, 0xDC);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xBFFF, 0x99);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xC000, 0x12);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xD001, 0x34);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xFFFF, 0x7F);
	EXPECT_THAT(result, IsEmptyResult());
	
	result = mapper.MapCPUAddressForWrite(0x1234, 0x56);
	EXPECT_THAT(result, IsEmptyResult());
	
	result = mapper.MapCPUAddressForWrite(0x0000, 0x00);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0x7FFF, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST(Mapper0Test, DualBankCPUWrite) {
	nes::Mapper0 mapper(2);

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0x00);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0x8123, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0x9876, 0xAB);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xABCD, 0xDC);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xBFFF, 0x99);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xC000, 0x12);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xD001, 0x34);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0xFFFF, 0x7F);
	EXPECT_THAT(result, IsEmptyResult());
	
	result = mapper.MapCPUAddressForWrite(0x1234, 0x56);
	EXPECT_THAT(result, IsEmptyResult());
	
	result = mapper.MapCPUAddressForWrite(0x0000, 0x00);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForWrite(0x7FFF, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST(Mapper0Test, PPURead) {
	nes::Mapper0 mapper(1);

	nes::Mapper::Result result;
	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));

	result = mapper.MapPPUAddressForRead(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());
}
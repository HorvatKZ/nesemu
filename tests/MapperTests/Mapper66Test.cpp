#include "MapperTestUtils.hpp"

#include "Mappers/Mapper66.hpp"


TEST(Mapper66Test, CPUReadOutOfRange) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());
}


TEST(Mapper66Test, CPUReadFirstBank) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x1ABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));

	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x4000));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x7FFF));
}


TEST(Mapper66Test, CPUReadSecondBank) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0x01);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x8000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x9ABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0xBFFF));

	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0xC000));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0xFFFF));
}


TEST(Mapper66Test, CPUReadThirdBankWithHiBitsSet) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0xFE);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x10000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x11ABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x13FFF));

	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x14000));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x17FFF));
}


TEST(Mapper66Test, CPUReadMaxBank) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x18000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x19ABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x1BFFF));

	result = mapper.MapCPUAddressForRead(0xC000);
	EXPECT_THAT(result, IsMappedAddress(0x1C000));

	result = mapper.MapCPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFFF));
}


TEST(Mapper66Test, CPUReadInvalidSet) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x7FFF, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapCPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapCPUAddressForRead(0x9ABC);
	EXPECT_THAT(result, IsMappedAddress(0x1ABC));

	result = mapper.MapCPUAddressForRead(0xBFFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));
}


TEST(Mapper66Test, PPUReadFirstBank) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));
}


TEST(Mapper66Test, PPUReadSecondBank) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0x10);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x2000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x3234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x3FFF));
}


TEST(Mapper66Test, PPUReadThirdBankWithHiBitsSet) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0xEF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x4000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x5234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x5FFF));
}


TEST(Mapper66Test, PPUReadMaxBank) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x8000, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x6000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x7234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x7FFF));
}


TEST(Mapper66Test, PPUReadInvalidSet) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapCPUAddressForWrite(0x7FFF, 0xFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x0000);
	EXPECT_THAT(result, IsMappedAddress(0x0000));

	result = mapper.MapPPUAddressForRead(0x1234);
	EXPECT_THAT(result, IsMappedAddress(0x1234));

	result = mapper.MapPPUAddressForRead(0x1FFF);
	EXPECT_THAT(result, IsMappedAddress(0x1FFF));
}


TEST(Mapper66Test, PPUReadOutOfRange) {
	nes::Mapper66 mapper;

	nes::Mapper::Result result;
	result = mapper.MapPPUAddressForRead(0x2000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x2001);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x3456);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x7FFF);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0x8000);
	EXPECT_THAT(result, IsEmptyResult());

	result = mapper.MapPPUAddressForRead(0xFFFF);
	EXPECT_THAT(result, IsEmptyResult());
}
#pragma once

#include <iostream>
#include <sstream>
#include <gmock/gmock.h>

#include "defines.h"
#include "Mapper.hpp"


template <typename UINT>
inline void PrintHex(UINT data, std::ostream* os, uint8 padding)
{
	*os << "0x" << std::hex << std::uppercase << std::setw(padding) << std::setfill('0') << data;
}


template <typename UINT>
std::string ToHexStr(UINT data, uint8 padding) {
	std::stringstream ss;
	PrintHex(data, &ss, padding);
	return ss.str();
}


namespace nes {
	inline void PrintTo(const Mapper::MappedAddress& ma, std::ostream* os)
	{
		*os << "MappedAddress { ";
		PrintHex(ma.address, os, 4);
		*os << " }";
	}

	inline void PrintTo(const Mapper::DataFromRAM& dfr, std::ostream* os)
	{
		*os << "DataFromRAM { ";
		PrintHex(dfr.data, os, 2);
		*os << " }";
	}

	inline void PrintTo(const Mapper::EmptyResult& er, std::ostream* os)
	{
		*os << "EmptyResult {}";
	}

	inline void PrintTo(const Mapper::Result& res, std::ostream* os)
	{
		std::visit([&os](const auto& arg) { PrintTo(arg, os); }, res);
	}
}


MATCHER(IsEmptyResult, "is EmptyResult")
{
	return std::holds_alternative<nes::Mapper::EmptyResult>(arg);
}


MATCHER_P(IsMappedAddress, address, "is MappedAddress { " + ToHexStr(address, 4) + " }")
{
	return std::holds_alternative<nes::Mapper::MappedAddress>(arg) && std::get<nes::Mapper::MappedAddress>(arg).address == address;
}


MATCHER_P(IsDataFromRAM, data, "is DataFromRAM { " + ToHexStr(data, 2) + " }")
{
	return std::holds_alternative<nes::Mapper::DataFromRAM>(arg) && std::get<nes::Mapper::DataFromRAM>(arg).data == data;
}
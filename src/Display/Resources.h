#pragma once

#define str(a) #a

#define RESOURCE(root, name, extension) str(root) "/" #name "." #extension

#define ROM(name) RESOURCE(RESOURCE_FOLDER, name, nes)
#define TXT(name) RESOURCE(RESOURCE_FOLDER, name, txt)

#include "FrameData.hpp"


namespace nes {

	std::vector<GLuint> FrameData::textureIDs;

	void FrameData::StartFrame()
	{
	}


	void FrameData::EndFrame()
	{
		glDeleteTextures(textureIDs.size(), textureIDs.data());
	}


	void FrameData::RegisterTextureID(GLuint id)
	{
		if (id != 0) {
			textureIDs.push_back(id);
		}
	}

}
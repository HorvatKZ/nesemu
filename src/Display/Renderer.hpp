#pragma once

#include <glad/glad.h>


namespace nes {

	struct Vertex
	{
		float x, y;
		float r, g, b;
	};


	class Renderer {
	public:
		Renderer();
		~Renderer();

		void StartFrame(int width, int height);
		void EndFrame();

		void DrawQuad(Vertex vertices[4]);

	private:
		GLuint program, vao, vbo, ib;
	};
}
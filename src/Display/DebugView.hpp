#pragma once

#include "Bus.hpp"
#include "AudioPlayer.hpp"


namespace nes {

	class DebugView {
	public:
		DebugView(Bus& bus, Audio::Player& audio);

		void Render();

		void StepBus();

	private:
		enum ControlMode {
			StepCPU1,
			StepCPU16,
			StepCPU256,
			StepPPU,
			StepFrame,
			RunFrame
		};

		Bus& bus;
		CPU& cpu;
		PPU& ppu;
		APU& apu;

		Audio::Player& audio;

		ControlMode controlMode = StepCPU1, prevControlMode = controlMode;
		uint16 dumpStartingLine = 0x0000;
		int32 palette = 0;
		uint8 oamStartingLine = 0;
		uint8 nameTableStartingLine = 0;
		uint8 volume = 50;
		bool pulse1Enabled = true, pulse2Enabled = true, triangleEnabled = true, noiseEnabled = true;

		void RenderDebugControl();
		void RenderAudioControl();
		void RenderCounts();
		void RenderControllers();
		void RenderCPUState();
		void RenderDisassembly();
		void RenderMemory();
		void RenderPPUState();
		void RenderPatternTables();
		void RenderPalettes();
		void RenderOAM();
		void RenderNametables();

		void StepCPUIntruction();
	};
}
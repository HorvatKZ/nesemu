#include "DebugView.hpp"
#include "Image.hpp"
#include "AudioPlayer.hpp"

#include <imgui.h>
#include <imgui_internal.h> // for disabling items

#include <algorithm>
#include <cstring>


namespace nes {

	static const int32 disassemblyLines = 5;
	static const int32 memoryLines = 8;
	static const int32 oamLines = 8;
	static const int32 nameTableLines = 16;


	DebugView::DebugView(Bus& bus, Audio::Player& audio) : bus(bus), cpu(bus.GetCPU()), ppu(bus.GetPPU()), apu(bus.GetAPU()), audio(audio)
	{
	}


	void DebugView::StepBus()
	{
		if (audio.IsEnabled()) {
			while (!ppu.IsFrameReady()) {}
			return;
		}

		const bool stepIfPressed = (controlMode != RunFrame);
		if (stepIfPressed && !ImGui::IsKeyPressed(ImGuiKey_Space, true)) {
			return;
		}

		switch (controlMode) {
			case StepCPU1:
				StepCPUIntruction();
				break;
			case StepCPU16:
				for (uint32 i = 0; i < 16; ++i) {
					StepCPUIntruction();
				}
				break;
			case StepCPU256:
				for (uint32 i = 0; i < 256; ++i) {
					StepCPUIntruction();
				}
				break;
			case StepPPU:
				bus.Clock();
				break;
			case StepFrame:
			case RunFrame:
				do {
					bus.Clock();
				} while (!ppu.frameJustCompleted);
				break;
		}
	}


	void DebugView::Render()
	{
		ImGui::Begin("DebugView", nullptr, ImGuiWindowFlags_NoNavInputs);

		RenderDebugControl();
		RenderAudioControl();
		RenderCounts();
		RenderControllers();
		RenderCPUState();
		RenderDisassembly();
		RenderMemory();
		RenderPPUState();
		RenderPatternTables();
		RenderPalettes();
		RenderOAM();
		RenderNametables();

		ImGui::End();
	}
	

	void DebugView::RenderDebugControl()
	{
		const char* items[] = { "Step 1 CPU intstruction", "Step 16 CPU intstruction", "Step 256 CPU intstruction", "Step 1 PPU clock cycles", "Step 1 frame", "Run" };
		ImGui::Combo("Mode", reinterpret_cast<int*>(&controlMode), items, IM_ARRAYSIZE(items));

		if (controlMode == RunFrame) {
			if (ImGui::Button("Stop")) {
				controlMode = StepCPU1;
			}
		} else {
			if (ImGui::Button("Run")) {
				controlMode = RunFrame;
			}
		}
		ImGui::SameLine();

		if (ImGui::Button("Reset")) {
			bus.Reset();
		}

		if (prevControlMode != RunFrame && controlMode == RunFrame) {
			audio.Enable();
		}
		if (prevControlMode == RunFrame && controlMode != RunFrame) {
			audio.Disable();
		}

		prevControlMode = controlMode;
	}


	void DebugView::RenderAudioControl()
	{
		if (!ImGui::CollapsingHeader("Audio:", ImGuiTreeNodeFlags_DefaultOpen)) {
			return;
		}

		if (audio.IsEnabled()) {
			if (ImGui::Button("ON ")) {
				audio.Disable();
			}
		} else if (controlMode == RunFrame) {
			if (ImGui::Button("OFF")) {
				audio.Enable();
			}
		} else {
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
			ImGui::Button("OFF");
			ImGui::PopStyleVar();
			ImGui::PopItemFlag();
		}
		ImGui::SameLine();

		static const uint8 volumeMin = 0;
		static const uint8 volumeMax = 100;
		ImGui::SliderScalar(" Volume", ImGuiDataType_U8, &volume, &volumeMin, &volumeMax, "%d");
		audio.SetVolume(volume / 100.f);

		ImGui::Text("Channels:");
		ImGui::Checkbox("Pulse 1", &pulse1Enabled);
		ImGui::Checkbox("Pulse 2", &pulse2Enabled);
		ImGui::Checkbox("Triangle", &triangleEnabled);
		ImGui::Checkbox("Noise", &noiseEnabled);

		apu.pulse1.SetForceDisable(!pulse1Enabled);
		apu.pulse2.SetForceDisable(!pulse2Enabled);
		apu.triangle1.SetForceDisable(!triangleEnabled);
		apu.noise1.SetForceDisable(!noiseEnabled);
	}


	void DebugView::RenderCPUState()
	{
		if (!ImGui::CollapsingHeader("CPU State:")) {
			return;
		}

        ImGui::Text("A: %02x, X: %02x, Y: %02x", cpu.accumulator, cpu.registerX, cpu.registerY);
        ImGui::Text("PC: $%04x, SP: %02x", cpu.programCounter, cpu.stackPointer);
        ImGui::Text("Flags: %c %c %c %c %c %c %c %c (%02x)",
			cpu.GetFlag(CPU::NegativeResult) ? 'N' : '_',
			cpu.GetFlag(CPU::Overflow) ? 'V' : '_',
			cpu.GetFlag(CPU::Unused) ? 'U' : '_',
			cpu.GetFlag(CPU::Break) ? 'B' : '_',
			cpu.GetFlag(CPU::DecimalMode) ? 'D' : '_',
			cpu.GetFlag(CPU::DisableInterrupt) ? 'I' : '_',
			cpu.GetFlag(CPU::ZeroResult) ? 'Z' : '_',
			cpu.GetFlag(CPU::CarryBit) ? 'C' : '_',
			cpu.status
		);
	}


	void DebugView::RenderDisassembly()
	{
		if (!ImGui::CollapsingHeader("Program:")) {
			return;
		}

		std::vector<std::string> disassembly = cpu.Disassemble(cpu.programCounter, std::min(256 * 256, (int32)cpu.programCounter + disassemblyLines * 3));
		for (uint32 i = 0; i < std::min(disassemblyLines, (int32)disassembly.size()); ++i) {
			if (i == 0) {
				ImGui::BulletText(disassembly[0].c_str());
			} else if (i < disassembly.size()) {
				ImGui::Text(disassembly[i].c_str());
			} else {
				ImGui::Text("");
			}
		}
	}


	void DebugView::RenderMemory()
	{
		if (!ImGui::CollapsingHeader("Memory:")) {
			return;
		}

		const uint16 dumpMin = 0x000;
		const uint16 dumpMax = 0x1FF - memoryLines + 1;

		ImGui::SliderScalar(" Location", ImGuiDataType_U16, &dumpStartingLine, &dumpMin, &dumpMax, "$%03x0");
		const uint16 memoryStartAt = dumpStartingLine * 16;
		for (int32 i = 0; i < memoryLines; ++i) {
			uint8 b[16];
			for (int32 j = 0; j < 16; ++j) {
				b[j] = cpu.Read(memoryStartAt + i * 16 + j);
			}
			ImGui::Text("$%04x: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x", memoryStartAt + i * 16,
				b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7], b[8], b[9], b[10], b[11], b[12], b[13], b[14], b[15]
			);
		}
	}


	void DebugView::RenderPPUState()
	{
		if (!ImGui::CollapsingHeader("PPU state:")) {
			return;
		}

		ImGui::Text("ScL: %03d, Cyc: %03d [%c]", ppu.scanline, ppu.cycle, ppu.IsWritingToScreenBuffer() ? 'X' : '_');

		ImGui::Text("Control (0x2000): %s %s %s %s %s %s %s %s (%02x)",
			ppu.GetFlag(PPU::EnableNonMaskableInterrupt) ? "NMI" : "___",
			ppu.GetFlag(PPU::SlaveMode) ? "SLM" : "___",
			ppu.GetFlag(PPU::LargeSprites) ? "S16" : "___",
			ppu.GetFlag(PPU::PatternBackground) ? "PBG" : "___",
			ppu.GetFlag(PPU::PatternSprite) ? "PFG" : "___",
			ppu.GetFlag(PPU::IncrementMode) ? "INC" : "___",
			ppu.GetFlag(PPU::NameTableY) ? "NTY" : "___",
			ppu.GetFlag(PPU::NameTableX) ? "NTX" : "___",
			ppu.control
		);

		ImGui::Text("Mask    (0x2001): %s %s %s %s %s %s %s %s (%02x)",
			ppu.GetFlag(PPU::EnhanceBlue) ? "ENB" : "___",
			ppu.GetFlag(PPU::EnhanceGreen) ? "ENG" : "___",
			ppu.GetFlag(PPU::EnhanceRed) ? "ENR" : "___",
			ppu.GetFlag(PPU::RenderSprites) ? "RFG" : "___",
			ppu.GetFlag(PPU::RenderBackground) ? "RBG" : "___",
			ppu.GetFlag(PPU::RenderSpritesLeft) ? "RFL" : "___",
			ppu.GetFlag(PPU::RenderBackgroundLeft) ? "RBL" : "___",
			ppu.GetFlag(PPU::Grayscale) ? "GRY" : "___",
			ppu.mask
		);

		ImGui::Text("Status  (0x2002): %s %s %s --- --- --- --- --- (%02x)",
			ppu.GetFlag(PPU::VerticalBlank) ? "VBL" : "___",
			ppu.GetFlag(PPU::SpirteZeroHit) ? "SZH" : "___",
			ppu.GetFlag(PPU::SpirteOverflow) ? "SOF" : "___",
			ppu.status
		);
	}


	void DebugView::RenderPatternTables()
	{
		if (!ImGui::CollapsingHeader("Pattern tables:", ImGuiTreeNodeFlags_DefaultOpen)) {
			return;
		}

		std::vector<PPU::Color> patternTable0Data = ppu.GetPatternTable(0, palette);
		Image patternTable0(128, 128, patternTable0Data);
		ImGui::Image(patternTable0.GetID(), ImVec2(256, 256));
		ImGui::SameLine();

		std::vector<PPU::Color> patternTable1Data = ppu.GetPatternTable(1, palette);
		Image patternTable1(128, 128, patternTable1Data);
		ImGui::Image(patternTable1.GetID(), ImVec2(256, 256));
	}


	void DebugView::RenderPalettes()
	{
		if (!ImGui::CollapsingHeader("Palettes:", ImGuiTreeNodeFlags_DefaultOpen)) {
			return;
		}

		char text[16] = { 0 };
		uint8 colorIndex = 0;
		for (uint8 i = 0; i < 8; ++i) {
			sprintf(text, "%d", i);
			ImGui::RadioButton(text, &palette, i); ImGui::SameLine();

			std::vector<PPU::Color> palette = ppu.GetPalette(i);
			for (const PPU::Color& color : palette) {
				sprintf(text, "%d-%d: 0x%02x", colorIndex / 4, colorIndex % 4, ppu.GetPaletteColorID(colorIndex % 4, colorIndex / 4));
				ImGui::ColorButton(text, ImVec4(color.r / 255.f, color.g / 255.f, color.b / 255.f, 1.f), ImGuiColorEditFlags_NoBorder, ImVec2(20, 20));
				ImGui::SameLine();
				++colorIndex;
			}
			ImGui::Text("  ");
			if (i % 4 != 3) {
				ImGui::SameLine();
			}
		}
	}


	void DebugView::RenderOAM()
	{
		if (!ImGui::CollapsingHeader("OAM:")) {
			return;
		}

		const uint8 slideMind = 0;
		const uint8 slideMax = 64 - oamLines;

		ImGui::SliderScalar(" Entry", ImGuiDataType_U8, &oamStartingLine, &slideMind, &slideMax, "%d");
		for (int32 i = 0; i < oamLines; ++i) {
			const PPU::ObjectAttributeEntry& entry = ppu.objectAttributeMemory[oamStartingLine + i];
			ImGui::Text("[%02d]: %02x (%03d, %03d) %c %c %c %d", oamStartingLine + i, entry.id, entry.x, entry.y,
				entry.attribute.verticalFlip ? 'V' : '_',
				entry.attribute.horizontalFlip ? 'H' : '_',
				entry.attribute.behindBackground ? 'B' : '_',
				entry.attribute.palette);
		}
	}


	void DebugView::RenderNametables()
	{
		if (!ImGui::CollapsingHeader("Nametables:")) {
			return;
		}

		const uint8 slideMind = 0;
		const uint8 slideMax = 64 - nameTableLines;

		ImGui::SliderScalar(" Line", ImGuiDataType_U8, &nameTableStartingLine, &slideMind, &slideMax, "%02x");
		ImGui::Text("  | 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 10 11 12 13 14 15 16 17 18 19 1A 1B 1C 1D 1E 1F");
		ImGui::Text("--| -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --");
		for (int32 i = 0; i < nameTableLines; ++i) {
			const uint8* nt = &(ppu.nameTables[nameTableStartingLine >> 5][(nameTableStartingLine % 32 + i) * 32]);
			ImGui::Text("%02x| %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x",
				nameTableStartingLine + i, nt[0], nt[1], nt[2], nt[3], nt[4], nt[5], nt[6], nt[7], nt[8], nt[9], nt[10], nt[11], nt[12], nt[13], nt[14], nt[15],
				nt[16], nt[17], nt[18], nt[19], nt[20], nt[21], nt[22], nt[23], nt[24], nt[25], nt[26], nt[27], nt[28], nt[29], nt[30], nt[31]
			);
		}
	}


	void DebugView::RenderControllers()
	{
		if (!ImGui::CollapsingHeader("Controllers:")) {
			return;
		}

		for (uint8 i = 0; i < 2; ++i) {
			ImGui::Text("Player %d: %c %c %c %c %c %c %c %c",
				i + 1,
				bus.IsControllerKeyPressed(i, Controller::Up) ? 'W' : '_',
				bus.IsControllerKeyPressed(i, Controller::Left) ? 'A' : '_',
				bus.IsControllerKeyPressed(i, Controller::Down) ? 'S' : '_',
				bus.IsControllerKeyPressed(i, Controller::Right) ? 'D' : '_',
				bus.IsControllerKeyPressed(i, Controller::Start) ? 'T' : '_',
				bus.IsControllerKeyPressed(i, Controller::Select) ? 'S' : '_',
				bus.IsControllerKeyPressed(i, Controller::A) ? 'A' : '_',
				bus.IsControllerKeyPressed(i, Controller::B) ? 'B' : '_'
			);
		}
	}

	void DebugView::RenderCounts()
	{
		if (!ImGui::CollapsingHeader("Counts:")) {
			return;
		}

		ImGui::Text("CPU clock: %d", cpu.clockCount);
		ImGui::Text("PPU clock: %d", ppu.clockCount);
		ImGui::Text("PPU frame: %d", ppu.frameCount);
	}


	void DebugView::StepCPUIntruction()
	{
		do {
			bus.Clock();
			bus.Clock();
			bus.Clock();
		} while (!cpu.justFinishedAnInstruction);
	}

}
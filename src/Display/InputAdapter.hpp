#pragma once

#include <vector>
#include <set>

#include <imgui.h>

#include "Controller.hpp"


namespace nes {

	class Bus;

	class InputAdapter {
	public:
		InputAdapter(Bus& bus);

		void UpdateContollers();

	private:
		Bus& bus;

		static std::vector<std::pair<Controller::Key, std::vector<ImGuiKey>>> keyMapping;
	};
}
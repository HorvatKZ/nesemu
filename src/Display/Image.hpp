#pragma once

#include "defines.h"
#include "PPU.hpp"

#include <glad/glad.h>
#include <imgui.h>

#include <vector>


namespace nes {

	class Image
	{
	public:
		Image(uint16 width, uint16 height, const std::vector<PPU::Color>& data);

		inline ImTextureID	GetID() const		{ return reinterpret_cast<ImTextureID>(id); }
		inline ImVec2		GetSize() const		{ return ImVec2(width, height); }
		inline uint16		GetWidth() const	{ return width; }
		inline uint16		GetHeight() const	{ return height; }

	private:
		GLuint id;
		uint16 width, height;

		void Load(const std::vector<PPU::Color>& data);
	};
}
#include "ViewPort.hpp"
#include "Image.hpp"

#include <imgui.h>


namespace nes {

	ViewPort::ViewPort(PPU& ppu) : ppu(ppu)
	{
	}


	void ViewPort::Render()
	{
		ImGui::Begin("ViewPort", nullptr, ImGuiWindowFlags_NoNavInputs);

		ImVec2 windowSize = ImGui::GetWindowSize();
		uint16 screenHeight = windowSize.y - 35;

		std::vector<PPU::Color> screenBuffer = ppu.GetScreenBuffer();
		Image screen(256, 240, screenBuffer);
		ImGui::Image(screen.GetID(), ImVec2(screenHeight / 240 * 256, screenHeight));

		ImGui::End();
	}

}
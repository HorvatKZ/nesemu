#pragma once

#include <glad/glad.h>

#include <vector>


namespace nes {

	class FrameData
	{
	public:
		static void StartFrame();
		static void EndFrame();

		static void RegisterTextureID(GLuint id);

	private:
		static std::vector<GLuint> textureIDs;
	};
}
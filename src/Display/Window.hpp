#pragma once

#include <functional>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include "Bus.hpp"

#include "DebugView.hpp"
#include "ViewPort.hpp"
#include "InputAdapter.hpp"


namespace nes {

	class Renderer;
	class DebugView;


	class Window {
	public:
		Window(Bus& bus, Audio::Player& audio);
		~Window();

		void PollEvents();
		void StartFrame(Renderer& renderer);
		void DoOneFrame();
		void EndFrame(Renderer& renderer);

		bool IsClosing();
		double GetTime();


	private:
		GLFWwindow* win = nullptr;
		DebugView debugView;
		ViewPort viewPort;
		InputAdapter inputAdapter;
		std::optional<std::string> unhandledError;

		Bus& bus;

		void StartDockspace();
		void EndDockspace();

		void RenderError();

		std::function<void(std::string)> GetErrorFunc();
	};
}
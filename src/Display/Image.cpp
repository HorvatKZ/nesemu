#include "Image.hpp"
#include "FrameData.hpp"


namespace nes{

	Image::Image(uint16 width, uint16 height, const std::vector<PPU::Color>& data) : width(width), height(height)
	{
		Load(data);
		FrameData::RegisterTextureID(id);
	}


	void Image::Load(const std::vector<PPU::Color>& data)
	{
		IM_ASSERT(data.size() == width * height);

		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data.data());
	}

}
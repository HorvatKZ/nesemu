#ifdef _WIN32
#include "AudioPlayer.hpp"

#include <Windows.h>
#include <mmsystem.h>
#include <memory>
#include <condition_variable>
#include <cmath>
#include <thread>


#define CHECKED_API_CALL(name, ...) { const auto ret = name(__VA_ARGS__); if (ret != MMSYSERR_NOERROR && errorFunc != nullptr) { errorFunc(#name " failed with code: " + std::to_string(ret)); }}

#endif


namespace nes::Audio {

#ifdef _WIN32
	class PlayerImpl {
	public:
		PlayerImpl(Bus& bus);
		~PlayerImpl();

		bool IsEnabled() const;
		void Enable();
		void Disable();
		void SetVolume(float volume);

		void SetErrorFunc(std::function<void(const std::string&)> errorFunc);

		void OnChunkDone();

	private:
		Bus& bus;

		HWAVEOUT device;
		WAVEHDR headers[chunkCount];
		int16 chunks[chunkCount][chunkSize];
		uint8 chunkIndex = 0;
		float globalTime = 0.0f;
		float volume = 1.0f;

		std::function<void(std::string)> errorFunc;

		std::atomic<bool> isAlive = true;
		std::atomic<bool> isEnabled = false;
		std::atomic<uint8> freeChunks = 0;
		std::condition_variable hasFreeChunks;
		std::mutex hasFreeChunksLock, fillNextChunkLock;
		std::thread soundThread;

		void Setup();
		void WaitForFreeChunk();
		void FillNextChunk();
		float GetNextSample();

		void SoundThreadFunc();

	};


	static std::unique_ptr<PlayerImpl> instance;


	void CALLBACK waveOutProc(HWAVEOUT hWaveOut, UINT uMsg, DWORD dwParam1, DWORD dwParam2)
	{
		if (uMsg == WOM_DONE && instance != nullptr) {
			instance->OnChunkDone();
		}
	}


	Player::Player(Bus& bus)
	{
		instance = std::make_unique<PlayerImpl>(bus);
	}


	Player::~Player()
	{
		instance.reset();
	}


	bool Player::IsEnabled() const
	{
		return instance->IsEnabled();
	}


	void Player::Enable()
	{
		instance->Enable();
	}


	void Player::Disable()
	{
		instance->Disable();
	}


	void Player::SetVolume(float volume)
	{
		instance->SetVolume(volume);
	}


	void Player::SetErrorFunc(std::function<void(const std::string&)> errorFunc)
	{
		instance->SetErrorFunc(errorFunc);
	}


	PlayerImpl::PlayerImpl(Bus& bus) : bus(bus)
	{
		Setup();
	}


	PlayerImpl::~PlayerImpl() {
		isAlive.store(false);
		hasFreeChunks.notify_all();
		if (soundThread.joinable()) {
			soundThread.join();
		}
	}


	bool PlayerImpl::IsEnabled() const
	{
		return isEnabled.load();
	}


	void PlayerImpl::Enable()
	{
		isEnabled.store(true);
	}


	void PlayerImpl::Disable()
	{
		isEnabled.store(false);
	}


	static float clip(float value, float min, float max)
	{
		return value < min ? min : (value > max ? max : value);
	}


	void PlayerImpl::SetVolume(float volume)
	{
		this->volume = clip(volume, 0.0f, 1.0f);
	}
	

	void PlayerImpl::SetErrorFunc(std::function<void(const std::string&)> errorFunc)
	{
		this->errorFunc = errorFunc;
	}


	void PlayerImpl::FillNextChunk()
	{
		if (headers[chunkIndex].dwFlags & WHDR_PREPARED) {
			CHECKED_API_CALL(waveOutUnprepareHeader, device, &headers[chunkIndex], sizeof(WAVEHDR));
		}

		for (int i = 0; i < chunkSize; ++i) {
			chunks[chunkIndex][i] = clip(GetNextSample() * volume, -1.0f, 1.0f) * 32767;
			globalTime += frameTime;
		}
		
		CHECKED_API_CALL(waveOutPrepareHeader, device, &headers[chunkIndex], sizeof(headers[chunkIndex]));
		CHECKED_API_CALL(waveOutWrite, device, &headers[chunkIndex], sizeof(headers[chunkIndex]));

		chunkIndex = (chunkIndex + 1) % chunkCount;
	}


	float PlayerImpl::GetNextSample()
	{
		APU& apu = bus.GetAPU();
		bus.Clock();
		while (!apu.JustFinishedSample()) {
			bus.Clock();
		}

		return apu.GetSample();
	}


	void PlayerImpl::Setup()
	{
		WAVEFORMATEX waveFormat;
		waveFormat.wFormatTag = WAVE_FORMAT_PCM;
		waveFormat.nSamplesPerSec = sampleRate;
		waveFormat.wBitsPerSample = sizeof(short) * 8;
		waveFormat.nChannels = channels;
		waveFormat.nBlockAlign = (waveFormat.wBitsPerSample / 8) * waveFormat.nChannels;
		waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
		waveFormat.cbSize = 0;

		memset(headers, 0, sizeof(headers));
		memset(chunks, 0, sizeof(chunks));

		CHECKED_API_CALL(waveOutOpen, &device, WAVE_MAPPER, &waveFormat, (DWORD_PTR)waveOutProc, (DWORD_PTR)0, CALLBACK_FUNCTION);

		CHECKED_API_CALL(waveOutSetVolume, device, 0xFFFFFFFF);

		for (uint8 i = 0; i < chunkCount; ++i) {
			headers[i].lpData = (CHAR*)chunks[i];
			headers[i].dwBufferLength = chunkSize * 2;
			CHECKED_API_CALL(waveOutPrepareHeader, device, &headers[i], sizeof(headers[i]));
			CHECKED_API_CALL(waveOutWrite, device, &headers[i], sizeof(headers[i]));
		}

		soundThread = std::thread(std::bind(&PlayerImpl::SoundThreadFunc, this));

		std::unique_lock<std::mutex> lock(hasFreeChunksLock);
		hasFreeChunks.notify_one();
	}


	void PlayerImpl::OnChunkDone()
	{
		++freeChunks;
		std::unique_lock<std::mutex> lock(hasFreeChunksLock);
		hasFreeChunks.notify_one();
	}


	void PlayerImpl::WaitForFreeChunk()
	{
		if (freeChunks.load() == 0) {
			std::unique_lock<std::mutex> lock(hasFreeChunksLock);
			while (freeChunks.load() == 0 && isAlive.load()) {
				hasFreeChunks.wait(lock);
			}
		}
	}


	void PlayerImpl::SoundThreadFunc()
	{
		while (isAlive.load()) {
			if (!isEnabled.load()) {
				continue;
			}

			WaitForFreeChunk();
			--freeChunks;
			FillNextChunk();
		}

		CHECKED_API_CALL(waveOutPause, device);
		CHECKED_API_CALL(waveOutClose, device);
	}

#else

	static bool isEnabled = false;


	Player::Player(std::function<float(float, float)> getNextSampleFunc, std::function<void(std::string)> errorFunc)
	{
	}


	Player::~Player()
	{
	}


	bool Player::IsEnabled() const
	{
		return isEnabled;
	}


	void Player::Enable()
	{
		isEnabled = true;
	}


	void Player::Disable()
	{
		isEnabled = false;
	}


	void Player::SetVolume(float volume)
	{
	}


	void Player::SetErrorFunc(std::function<void(const std::string&)> errorFunc)
	{
	}

#endif

}
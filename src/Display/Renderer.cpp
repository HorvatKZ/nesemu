#include "Renderer.hpp"

#include <glm/glm.hpp>


namespace nes {

	// TODO move to assest
	static const char* vertShaderText =
		"#version 450 core\n"
		"layout(location = 0) in vec2 vs_in_pos;\n"
		"layout(location = 1) in vec3 vs_in_col;\n"
		"layout(location = 0) out vec3 vs_out_col;\n"
		"void main()\n"
		"{\n"
		"	vs_out_col = vs_in_col;\n"
		"	gl_Position = vec4(vs_in_pos, 1.0, 1.0);\n"
		"}\n";

	static const char* fragShaderText =
		"#version 450 core\n"
		"layout(location = 0) in vec3 fs_in_col;\n"
		"layout(location = 0) out vec4 fs_out_col;\n"
		"void main()\n"
		"{\n"
		"    fs_out_col = vec4(fs_in_col, 1.0);\n"
		"}\n";


	Renderer::Renderer()
	{
		gladLoadGL();

		GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertShader, 1, &vertShaderText, NULL);
		glCompileShader(vertShader);

		GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragShader, 1, &fragShaderText, NULL);
		glCompileShader(fragShader);

		program = glCreateProgram();
		glAttachShader(program, vertShader);
		glAttachShader(program, fragShader);
		glLinkProgram(program);

		glDeleteShader(vertShader);
		glDeleteShader(fragShader);

		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ib);
		if (vao == 0 || vbo == 0 || ib == 0) {
			exit(EXIT_FAILURE);
		}

		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 4, nullptr, GL_DYNAMIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, x));
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, r));

		GLuint indices[] = { 0, 2, 1, 0, 3, 2 };
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * 6, (void*)indices, GL_STATIC_DRAW);

		glBindVertexArray(0);
	}


	Renderer::~Renderer()
	{
		glDeleteVertexArrays(1, &vao);
		glDeleteBuffers(1, &vbo);
		glDeleteBuffers(1, &ib);
		glDeleteProgram(program);
	}


	void Renderer::StartFrame(int width, int height)
	{
		glUseProgram(program);
		glClearColor(0.f, 0.f, 0.2f, 1.f);
		glClear(GL_COLOR_BUFFER_BIT);

		glEnable(GL_BLEND);
		glEnable(GL_CULL_FACE);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glViewport(0, 0, width, height);
	}


	void Renderer::EndFrame()
	{
		glFlush();
		glUseProgram(0);
	}


	void Renderer::DrawQuad(Vertex vertices[4])
	{
		glBindVertexArray(vao);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * 4, vertices);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
}
#pragma once

#include <functional>
#include <string>

#include "defines.h"
#include "Bus.hpp"

namespace nes::Audio {

	constexpr uint32 sampleRate = 44100;
	constexpr uint32 chunkSize = 512;
	constexpr uint8 chunkCount = 8;
	constexpr uint8 channels = 1;
	constexpr float frameTime = 1.f / sampleRate;


	class Player {
	public:
		Player(Bus& bus);
		~Player();

		bool IsEnabled() const;
		void Enable();
		void Disable();
		void SetVolume(float volume);

		void SetErrorFunc(std::function<void (const std::string&)> errorFunc);
	};

}
#include "InputAdapter.hpp"
#include "Bus.hpp"

#include <algorithm>


namespace nes {

    std::vector<std::pair<Controller::Key, std::vector<ImGuiKey>>> InputAdapter::keyMapping = {
        { Controller::Up,       { ImGuiKey_UpArrow, ImGuiKey_W, ImGuiKey_GamepadDpadUp }},
        { Controller::Down,     { ImGuiKey_DownArrow, ImGuiKey_S, ImGuiKey_GamepadDpadDown }},
        { Controller::Left,     { ImGuiKey_LeftArrow, ImGuiKey_A, ImGuiKey_GamepadDpadLeft }},
        { Controller::Right,    { ImGuiKey_RightArrow, ImGuiKey_D, ImGuiKey_GamepadDpadRight }},
        { Controller::Start,    { ImGuiKey_1, ImGuiKey_Enter, ImGuiKey_GamepadStart }},
        { Controller::Select,   { ImGuiKey_2, ImGuiKey_RightShift, ImGuiKey_GamepadBack }},
        { Controller::A,        { ImGuiKey_3, ImGuiKey_Keypad1, ImGuiKey_GamepadFaceDown }},
        { Controller::B,        { ImGuiKey_4, ImGuiKey_Keypad2, ImGuiKey_GamepadFaceRight }}
    };


	InputAdapter::InputAdapter(Bus& bus) : bus(bus)
	{
	}


    void InputAdapter::UpdateContollers()
    {
        for (const auto& [nesKey, imGuiKeys] : keyMapping) {
            bool anyPressed = std::find_if(imGuiKeys.begin(), imGuiKeys.end(), ImGui::IsKeyDown) != imGuiKeys.end();
            bus.UpdateControllerKey(nesKey, anyPressed);
        }
    }

}
#pragma once

#include "PPU.hpp"


namespace nes {

	class ViewPort {
	public:
		ViewPort(PPU& ppu);

		void Render();
	private:
		PPU& ppu;
	};

}
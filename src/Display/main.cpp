#include "Window.hpp"
#include "DebugView.hpp"
#include "Renderer.hpp"
#include "AudioPlayer.hpp"
#include "Resources.h"
#include "Bus.hpp"

#include <fstream>


void DumpRam(const nes::CPU& cpu, const std::string& file)
{
    std::vector<std::string> disassembly = cpu.Disassemble(0x8000, 0xFFFF);
  
    std::ofstream fout(file);

    for (const std::string& entry : disassembly) {
        fout << entry << '\n';
    }

    fout.close();
}


int main(void)
{
    std::unique_ptr<nes::Bus> bus = std::make_unique<nes::Bus>(nes::Audio::sampleRate);

    nes::Audio::Player player (*bus);
    nes::Window window(*bus, player);
    nes::Renderer renderer;

    bus->LoadCartridge(ROM(nestest));
    bus->Reset();

    while (!window.IsClosing()) {
        window.StartFrame(renderer);

        window.DoOneFrame();

        window.EndFrame(renderer);
        window.PollEvents();
    }
}
#pragma once

#include "defines.h"

#include <variant>


namespace nes {

	enum class MirroringMode {
		HORIZONTAL,
		VERTICAL,
		ONESCREEN_LO,
		ONESCREEN_HI,
		HARDWARE_DETEMINED
	};


	class Mapper
	{
	public:
		struct MappedAddress {
			uint32 address;
		};

		struct DataFromRAM {
			uint8 data;
		};

		using EmptyResult = std::monostate;
		using Result = std::variant<MappedAddress, DataFromRAM, EmptyResult>;


		virtual ~Mapper() = default;

		virtual Result MapCPUAddressForRead(uint16 address) const = 0;
		virtual Result MapCPUAddressForWrite(uint16 address, uint8 value) = 0;
		virtual Result MapPPUAddressForRead(uint16 address) const = 0;
		virtual Result MapPPUAddressForWrite(uint16 address) { return EmptyResult{}; }

		virtual void Reset() {}

		virtual MirroringMode GetMirroring() const { return MirroringMode::HARDWARE_DETEMINED; }
	};

}
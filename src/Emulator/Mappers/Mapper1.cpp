#include "Mapper1.hpp"


namespace nes {

	Mapper1::Mapper1(uint8 programROMChunks, bool noCharacterROMChunks) : programROMChunks(programROMChunks), noCharacterROMChunks(noCharacterROMChunks)
	{
		Reset();
	}


	Mapper::Result Mapper1::MapCPUAddressForRead(uint16 address) const
	{
		if (0x6000 <= address && address < 0x8000) {
			return DataFromRAM{ ram[address & 0x1FFF] };
		} else if (0x8000 <= address) {
			if (controlRegister.programROM16KMode) {
				const bool loRange = address < 0xC000;
				const bool useFixedBank = loRange ^ controlRegister.programROMHiFixed;
				const uint8 bankToUse = useFixedBank ? programROMChunks - 1 : selectedProgramROMChunk;
				return MappedAddress{ (static_cast<uint32>(bankToUse) << 14) | (address & 0x3FFF) };
			} else {
				return MappedAddress{ (static_cast<uint32>(selectedProgramROMChunk) << 15) | (address & 0x7FFF) };
			}
		}

		return EmptyResult{};
	}


	Mapper::Result Mapper1::MapCPUAddressForWrite(uint16 address, uint8 value)
	{
		if (0x6000 <= address && address < 0x8000) {
			ram[address & 0x1FFF] = value;
			return DataFromRAM{ value };
		} if (0x8000 <= address) {
			if (value & 0x80) {
				loadRegister = 0;
				loadCount = 0;
				controlRegister.reg = controlRegister.reg | 0x0C;
				return EmptyResult{};
			}

			loadRegister = (loadRegister >> 1) | ((value & 1) << 4);
			++loadCount;

			if (loadCount == 5) {
				SaveLoadRegister(address);
				loadRegister = 0;
				loadCount = 0;
			}
		}

		return EmptyResult{};
	}


	Mapper::Result Mapper1::MapPPUAddressForRead(uint16 address) const
	{
		if (address >= 0x2000) {
			return EmptyResult{};
		}

		if (noCharacterROMChunks) {
			return MappedAddress{ address };
		}
			
		if (controlRegister.characterROM4KMode) {
			if (address < 0x1000) {
				return MappedAddress{ (static_cast<uint32>(selectedCharacterROMChunkLo) << 12) | (address & 0x0FFF) };
			} else {
				return MappedAddress{ (static_cast<uint32>(selectedCharacterROMChunkHi) << 12) | (address & 0x0FFF) };
			}
		} else {
			return MappedAddress{ (static_cast<uint32>(selectedCharacterROMChunkLo) << 13) | (address & 0x1FFF) };
		}
	}


	Mapper::Result Mapper1::MapPPUAddressForWrite(uint16 address)
	{
		if (address < 0x2000 && noCharacterROMChunks) {
			return MappedAddress{ address };
		}

		return EmptyResult{};
	}


	void Mapper1::Reset()
	{
		controlRegister.reg = 0x1C;
		selectedCharacterROMChunkLo = 0;
		selectedCharacterROMChunkHi = 0;
		selectedProgramROMChunk = 0;

		loadRegister = 0;
		loadCount = 0;

		memset(ram, 0, sizeof(ram));
	}


	MirroringMode Mapper1::GetMirroring() const
	{
		switch (controlRegister.mirroringMode) {
			case 0: return MirroringMode::ONESCREEN_LO;
			case 1: return MirroringMode::ONESCREEN_HI;
			case 2: return MirroringMode::VERTICAL;
			case 3: return MirroringMode::HORIZONTAL;
		}
	}


	void Mapper1::SaveLoadRegister(uint16 address)
	{
		if (0x8000 <= address && address < 0xA000) {
			controlRegister.reg = loadRegister & 0x1F;
		} else if (0xA000 <= address && address < 0xC000) {
			if (controlRegister.characterROM4KMode) {
				selectedCharacterROMChunkLo = loadRegister & 0x1F;
			} else {
				selectedCharacterROMChunkLo = (loadRegister & 0x1E) >> 1;
			}
		} else if (0xC000 <= address && address < 0xE000) {
			if (controlRegister.characterROM4KMode) {
				selectedCharacterROMChunkHi = loadRegister & 0x1F;
			}
		} else if (0xE000 <= address) {
			if (controlRegister.programROM16KMode) {
				selectedProgramROMChunk = loadRegister & 0x0F;
			} else {
				selectedProgramROMChunk = (loadRegister & 0x0E) >> 1;
			}
		}
	}
}
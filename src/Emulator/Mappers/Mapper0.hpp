#pragma once

#include "Mapper.hpp"


namespace nes {

	class Mapper0 final : public Mapper
	{
	public:
		Mapper0(uint8 programROMChunks);

		virtual Result MapCPUAddressForRead(uint16 address) const override;
		virtual Result MapCPUAddressForWrite(uint16 address, uint8 value) override;
		virtual Result MapPPUAddressForRead(uint16 address) const override;

	private:
		uint8 programROMChunks;
	};

}
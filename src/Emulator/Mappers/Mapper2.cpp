#include "Mapper2.hpp"


namespace nes {

	Mapper2::Mapper2(uint8 programROMChunks) : programROMChunks(programROMChunks)
	{
		Reset();
	}


	Mapper::Result Mapper2::MapCPUAddressForRead(uint16 address) const
	{
		if (0x8000 <= address && address < 0xC000) {
			return MappedAddress{ (static_cast<uint32>(selectedProgramROMChunk) << 14) | (address & 0x3FFF) };
		} else if (0xC000 <= address) {
			return MappedAddress{ (static_cast<uint32>(programROMChunks - 1) << 14) | (address & 0x3FFF) };
		}

		return EmptyResult{};
	}


	Mapper::Result Mapper2::MapCPUAddressForWrite(uint16 address, uint8 value)
	{
		if (0x8000 <= address) {
			selectedProgramROMChunk = value & 0x0F;
		}

		return EmptyResult{};
	}


	Mapper::Result Mapper2::MapPPUAddressForRead(uint16 address) const
	{
		if (address < 0x2000) {
			return MappedAddress{ address };
		}

		return EmptyResult{};
	}


	void Mapper2::Reset()
	{
		selectedProgramROMChunk = 0;
	}
}
#pragma once

#include "Mapper.hpp"


namespace nes {

	class Mapper66 final : public Mapper
	{
	public:
		Mapper66();

		virtual Result MapCPUAddressForRead(uint16 address) const override;
		virtual Result MapCPUAddressForWrite(uint16 address, uint8 value) override;
		virtual Result MapPPUAddressForRead(uint16 address) const override;

		virtual void Reset() override;

	private:
		uint8 selectedProgramROMChunk;
		uint8 selectedCharacterROMChunk;
	};

}
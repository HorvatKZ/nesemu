#include "Mapper66.hpp"


namespace nes {

	Mapper66::Mapper66()
	{
		Reset();
	}


	Mapper::Result Mapper66::MapCPUAddressForRead(uint16 address) const
	{
		if (0x8000 <= address) {
			return MappedAddress{ (static_cast<uint32>(selectedProgramROMChunk) << 15) | (address & 0x7FFF) };
		}

		return EmptyResult{};
	}


	Mapper::Result Mapper66::MapCPUAddressForWrite(uint16 address, uint8 value)
	{
		if (0x8000 <= address) {
			selectedProgramROMChunk = value & 0x03;
			selectedCharacterROMChunk = (value & 0x30) >> 4;
		}

		return EmptyResult{};
	}


	Mapper::Result Mapper66::MapPPUAddressForRead(uint16 address) const
	{
		if (address < 0x2000) {
			return MappedAddress{ (static_cast<uint32>(selectedCharacterROMChunk) << 13) | address };
		}

		return EmptyResult{};
	}


	void Mapper66::Reset()
	{
		selectedProgramROMChunk = 0;
		selectedCharacterROMChunk = 0;
	}
}
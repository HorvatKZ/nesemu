#include "Mapper0.hpp"


namespace nes {

	Mapper0::Mapper0(uint8 programROMChunks) : programROMChunks(programROMChunks)
	{
	}


	Mapper::Result Mapper0::MapCPUAddressForRead(uint16 address) const
	{
		if (0x8000 <= address) {
			return MappedAddress{ address & (programROMChunks > 1 ? 0x7FFFu : 0x3FFFu) };
		}

		return EmptyResult{};
	}


	Mapper::Result Mapper0::MapCPUAddressForWrite(uint16 address, uint8 value)
	{
		return EmptyResult{};
	}


	Mapper::Result Mapper0::MapPPUAddressForRead(uint16 address) const
	{
		if (address < 0x2000) {
			return MappedAddress{ address };
		}

		return EmptyResult{};
	}
}
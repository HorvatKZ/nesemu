#include "Mapper3.hpp"


namespace nes {

	Mapper3::Mapper3(uint8 programROMChunks) : programROMChunks(programROMChunks)
	{
		Reset();
	}


	Mapper::Result Mapper3::MapCPUAddressForRead(uint16 address) const
	{
		if (0x8000 <= address) {
			return MappedAddress{ address & (programROMChunks > 1 ? 0x7FFFu : 0x3FFFu) };
		}

		return EmptyResult{};
	}


	Mapper::Result Mapper3::MapCPUAddressForWrite(uint16 address, uint8 value)
	{
		if (0x8000 <= address) {
			selectedCharacterROMChunk = value & 0x03;
		}

		return EmptyResult{};
	}


	Mapper::Result Mapper3::MapPPUAddressForRead(uint16 address) const
	{
		if (address < 0x2000) {
			return MappedAddress{ (static_cast<uint32>(selectedCharacterROMChunk) << 13) | address };
		}

		return EmptyResult{};
	}


	void Mapper3::Reset()
	{
		selectedCharacterROMChunk = 0;
	}
}
#pragma once

#include "Mapper.hpp"


namespace nes {

	class Mapper1 final : public Mapper
	{
	public:
		Mapper1(uint8 programROMChunks, bool noCharacterROMChunks);

		virtual Result MapCPUAddressForRead(uint16 address) const override;
		virtual Result MapCPUAddressForWrite(uint16 address, uint8 value) override;
		virtual Result MapPPUAddressForRead(uint16 address) const override;
		virtual Result MapPPUAddressForWrite(uint16 address) override;

		virtual void Reset() override;

		virtual MirroringMode GetMirroring() const override;

	private:
		union ControlRegister {
			struct {
				uint8 mirroringMode : 2;
				uint8 programROMHiFixed : 1;
				uint8 programROM16KMode : 1;
				uint8 characterROM4KMode : 1;
			};

			uint8 reg = 0x00;
		};

		uint8 programROMChunks;
		bool noCharacterROMChunks;

		ControlRegister controlRegister;
		uint8 selectedCharacterROMChunkLo;
		uint8 selectedCharacterROMChunkHi;
		uint8 selectedProgramROMChunk;

		uint8 loadRegister;
		uint8 loadCount;

		uint8 ram[32 * 1024];

		void SaveLoadRegister(uint16 address);
	};

}
#include "PPU.hpp"


namespace nes {

	static const uint16 resolutionWidth = 256;
	static const uint16 resolutionHeight = 240;

	static const uint16 firstRenderScanline = 1;
	static const uint16 lastRenderScanline = firstRenderScanline + resolutionHeight - 1;
	static const uint16 vblankScanline = lastRenderScanline + 2;
	static const uint16 lastScanline = 261;

	static const uint16 firstRenderCycle = 1;
	static const uint16 firstLoadCycle = firstRenderCycle + 1;
	static const uint16 lastRenderCycle = firstRenderCycle + resolutionWidth - 1;
	static const uint16 lastLoadCycle = lastRenderCycle + 1;
	static const uint16 firstTransferYCycle = 280;
	static const uint16 lastTransferYCycle = 304;
	static const uint16 first2ndPhaseLoadCycle = 321;
	static const uint16 after2ndPhaseLoadCycles = 338;
	static const uint16 lastCycle = 340;


	static uint8 mirroringConfig[][4] = {
		{ 0, 0, 1, 1 },
		{ 0, 1, 0, 1 },
	};


	static uint8 MirrorByte(uint8 b)
	{
		// https://stackoverflow.com/a/2602885
		b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
		b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
		b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
		return b;
	}


	PPU::PPU(Bus& bus) : bus(bus),
		bgRenderer(*this), fgRenderer(*this), screenBuffer(resolutionWidth * resolutionHeight), colors(64),
		oamByteAccessor(reinterpret_cast<uint8*> (objectAttributeMemory))
#ifdef LOG_PPU
		, logger("ppudump.txt")
#endif
	{
		memset(nameTables, 0, sizeof(nameTables));
		memset(paletteTable, 0, sizeof(paletteTable));
		memset(objectAttributeMemory, 0, sizeof(objectAttributeMemory));
		FillColors();
	}


	void PPU::Clock()
	{
		frameJustCompleted = false;

		if (scanline == 0 && cycle == firstRenderCycle) {
			SetFlag(VerticalBlank, false);
			SetFlag(SpirteOverflow, false);
			SetFlag(SpirteZeroHit, false);
		} else if (scanline == firstRenderScanline && cycle == 0) {
			cycle = 1; // Skip odd frame
		} else if (scanline == 242 && cycle == firstRenderCycle) {
			SetFlag(VerticalBlank, true);
			if (GetFlag(EnableNonMaskableInterrupt)) {
				emitNonMaskableInterrupt = true;
			}
		}

		BackgroundRenderer::CycleResult bgColor = bgRenderer.Cycle(scanline, cycle);
		ForegroundRenderer::CycleResult fgColorInfo = fgRenderer.Cycle(scanline, cycle);
		if (IsWritingToScreenBuffer()) {
			uint16 screenX = cycle - 1;
			uint16 screenY = scanline - 1;
			screenBuffer[screenY * resolutionWidth + screenX] = MergeColors(bgColor, fgColorInfo);
		}

		++cycle;
		if (cycle == lastCycle + 1) {
			cycle = 0;
			++scanline;
			if (scanline == lastScanline + 1) {
				scanline = 0;
				++frameCount;
				frameJustCompleted = true;
			}
		}

		++clockCount;
	}


	uint8 PPU::CPURead(uint16 address)
	{
		uint8 result = 0;
		switch (static_cast<Interfaces>(address)) {
			case Interfaces::Status:
				result = status | (dataBuffer & 0x1F);
				SetFlag(VerticalBlank, false);
				dataAddressHiByte = true;
				break;
			case Interfaces::OAM_Data:
				result = oamByteAccessor[oamAddress];
				break;
			case Interfaces::Data:
				result = dataBuffer;
				dataBuffer = Read(vramAddress.reg);
				if (vramAddress.reg >= 0x3F00) {
					result = dataBuffer;
				}
				vramAddress.reg += GetFlag(IncrementMode) ? 32 : 1;
				break;
		}
		return result;
	}


	void PPU::CPUWrite(uint16 address, uint8 value)
	{
#ifdef LOG_PPU
		logger.Log("CPU: %04X: %02X", address, value);
#endif
		switch (static_cast<Interfaces>(address)) {
			case Interfaces::Control:
				control = value;
				tempVramAddress.nametableX = GetFlag(NameTableX);
				tempVramAddress.nametableY = GetFlag(NameTableY);
				break;
			case Interfaces::Mask:
				mask = value;
				break;
			case Interfaces::OAM_Address:
				oamAddress = value;
				break;
			case Interfaces::OAM_Data:
				oamByteAccessor[oamAddress] = value;
				break;
			case Interfaces::Scroll:
				if (dataAddressHiByte) {
					fineX = value & 0x07;
					tempVramAddress.coarseX = value >> 3;
				} else {
					tempVramAddress.fineY = value & 0x07;
					tempVramAddress.coarseY = value >> 3;
				}
				dataAddressHiByte = !dataAddressHiByte;
				break;
			case Interfaces::Address:
				if (dataAddressHiByte) {
					tempVramAddress.reg = (uint16)((value & 0x3F) << 8) | (tempVramAddress.reg & 0x00FF);
				} else {
					tempVramAddress.reg = (tempVramAddress.reg & 0xFF00) | value;
					vramAddress.reg = tempVramAddress.reg;
				}
				dataAddressHiByte = !dataAddressHiByte;
				break;
			case Interfaces::Data:
				Write(vramAddress.reg, value);
#ifdef LOG_PPU
				logger.Log("PPU: %04X: %02X", vramAddress.reg, value);
#endif
				vramAddress.reg += GetFlag(IncrementMode) ? 32 : 1;
				break;
		}
	}


	void PPU::WriteToOAM(uint8 address, uint8 value)
	{
		oamByteAccessor[address] = value;
	}


	void PPU::ConnectCartridge(const std::shared_ptr<Cartridge>& cartridge)
	{
		this->cartridge = cartridge;
	}


	std::vector<PPU::Color> PPU::GetPatternTable(uint8 index, uint8 palette) const
	{
		std::vector<Color> table(128 * 128);

		for (uint16 row = 0; row < 16; ++row) {
			for (uint16 col = 0; col < 16; ++col) {
				uint16 imageInd = row * 16 + col;
				for (uint16 y = 0; y < 8; ++y) {
					uint8 lsb = Read(index * 0x1000 + imageInd * 16 + y);
					uint8 msb = Read(index * 0x1000 + imageInd * 16 + y + 8);
					for (uint16 x = 0, currBit = 1; x < 8; ++x, currBit <<= 1) {
						uint8 currLsb = (lsb & (uint8)currBit) >> x;
						uint8 currMsb = (msb & (uint8)currBit) >> x;
						uint8 pixelValue = currLsb | (currMsb << 1);
						table[(row * 8 + y) * 128 + col * 8 + (7 - x) + 0] = GetPaletteColor(pixelValue, palette);
					}
				}
			}
		}

		return table;
	}


	std::vector<PPU::Color> PPU::GetPalette(uint8 index) const
	{
		return {
			GetPaletteColor(0, index),
			GetPaletteColor(1, index),
			GetPaletteColor(2, index),
			GetPaletteColor(3, index)
		};
	}


	bool PPU::EmitNonMaskableInterrupt()
	{
		bool result = emitNonMaskableInterrupt;
		emitNonMaskableInterrupt = false;
		return result;
	}


	bool PPU::IsWritingToScreenBuffer()
	{
		return firstRenderCycle <= cycle && cycle <= lastRenderCycle && firstRenderScanline <= scanline && scanline <= lastRenderScanline;
	}


	bool PPU::IsFrameReady()
	{
		return firstRenderScanline > scanline || scanline > lastRenderScanline;
	}


	void PPU::Reset()
	{
		bgRenderer.Reset();
		fgRenderer.Reset();

		status = 0;
		mask = 0;
		control = 0;
		dataAddressHiByte = true;
		dataBuffer = 0;
		vramAddress.reg = 0;
		tempVramAddress.reg = 0;
		fineX = 0;
		oamAddress = 0;
		frameJustCompleted = false;
		emitNonMaskableInterrupt = false;
		cycle = 0;
		scanline = 1;
		frameCount = 0;
		clockCount = 0;
	}


	uint8 PPU::Read(uint16 address) const
	{
		address &= 0x3FFF;

		if (cartridge != nullptr && address < 0x2000) {
			return cartridge->PPURead(address);
		} else if (0x2000 <= address && address < 0x3F00) {
			uint16 nameTableAddress = address & 0x03FF;
			uint16 nameTableIndex = (address & 0x0C00) >> 10;
			uint16 realNameTableIndex = mirroringConfig[static_cast<uint8> (cartridge->GetMirroring())][nameTableIndex];
			return nameTables[realNameTableIndex][nameTableAddress];
		} else if (0x3F00 <= address) {
			uint16 paletteAddress = address & 0x001F;
			const bool isBackgroundColor = (paletteAddress % 4 == 0);
			if (isBackgroundColor) {
				paletteAddress = 0;
			}
			return paletteTable [paletteAddress] ;
		}

		return 0;
	}


	void PPU::Write(uint16 address, uint8 value)
	{
		address &= 0x3FFF;

		if (cartridge != nullptr && address < 0x2000) {
			cartridge->PPUWrite(address, value);
		} else if (0x2000 <= address && address < 0x3F00) {
			uint16 nameTableAddress = address & 0x03FF;
			uint16 nameTableIndex = (address & 0x0C00) >> 10;
			uint16 realNameTableIndex = mirroringConfig[static_cast<uint8> (cartridge->GetMirroring())][nameTableIndex];
			nameTables[realNameTableIndex][nameTableAddress] = value;
		} else if (0x3F00 <= address) {
			uint16 paletteAddress = address & 0x001F;
			const bool isBackgroundColor = (paletteAddress % 4 == 0);
			if (isBackgroundColor && (paletteAddress & 0x000F) == 0) {
				paletteTable[0] = value;
				return;
			}
			paletteTable[paletteAddress] = value;
		}
	}

	bool PPU::GetFlag(StatusFlag flag) const
	{
		return status & flag;
	}


	bool PPU::GetFlag(MaskFlag flag) const
	{
		return mask & flag;
	}


	bool PPU::GetFlag(ControlFlag flag) const
	{
		return control & flag;
	}


	void PPU::SetFlag(StatusFlag flag, bool value)
	{
		if (value) {
			status |= flag;
		} else {
			status &= ~flag;
		}
	}


	void PPU::SetFlag(MaskFlag flag, bool value)
	{
		if (value) {
			mask |= flag;
		} else {
			mask &= ~flag;
		}
	}


	void PPU::SetFlag(ControlFlag flag, bool value)
	{
		if (value) {
			control |= flag;
		} else {
			control &= ~flag;
		}
	}


	PPU::Color PPU::GeretatePixel(uint16 x, uint16 y)
	{
		return {
			static_cast<uint8>(frameCount + x + y),
			static_cast<uint8>(frameCount - x + y),
			static_cast<uint8>(frameCount - x - y)
		};
	}


	PPU::Color PPU::GetPaletteColor(uint8 pixelValue, uint8 palette) const
	{
		return colors[GetPaletteColorID(pixelValue, palette)];
	}


	uint8 PPU::GetPaletteColorID(uint8 pixelValue, uint8 palette) const
	{
		return Read(0x3F00 + palette * 4 + pixelValue) & 0x3F;
	}


	PPU::Color PPU::MergeColors(const BackgroundRenderer::CycleResult& bg, const ForegroundRenderer::CycleResult& fg)
	{
		if (!fg.has_value()) {
			return bg.color;
		}

		if (bg.isBackgroundColor) {
			return fg->first;
		}

		if (fgRenderer.IsSpriteZeroHit(cycle)) {
			SetFlag(SpirteZeroHit, true);
		}

		switch (fg->second) {
			case ForegroundRenderer::Background:
				return bg.color;
			case ForegroundRenderer::Foreground:
				return fg->first;
		}
	}


	// === BackgroundRenderer ===

	PPU::BackgroundRenderer::BackgroundRenderer(PPU& ppu) : ppu(ppu)
	{
	}


	PPU::BackgroundRenderer::CycleResult PPU::BackgroundRenderer::Cycle(uint16 scanline, uint16 cycle)
	{
		if (scanline <= lastRenderScanline) {
			if ((firstLoadCycle <= cycle && cycle <= lastLoadCycle) || (first2ndPhaseLoadCycle <= cycle && cycle < after2ndPhaseLoadCycles)) {
				LoadCycle((cycle - 1) % 8);
			}

			if (cycle == lastRenderCycle) {
				IncrementScrollY();
			}
			if (cycle == lastLoadCycle) {
				LoadShifters();
				TransferAddressX();
			}

			if (cycle == after2ndPhaseLoadCycles || cycle == lastCycle) {
				nextTileId = ppu.Read(0x2000 | (ppu.vramAddress.reg & 0x0FFF));
			}

			if (scanline == 0 && cycle >= firstTransferYCycle && cycle <= lastTransferYCycle) {
				TransferAddressY();
			}
		}

		return CalculateColor();
	}

	void PPU::BackgroundRenderer::Reset()
	{
		nextTileId = 0;
		nextTilePaletteId = 0;
		nextTileLSBs = 0;
		nextTileMSBs = 0;
		patternShifterLo = 0;
		patternShifterHi = 0;
		attributeShifterLo = 0;
		attributeShifterHi = 0;
	}

	void PPU::BackgroundRenderer::LoadCycle(uint8 index)
	{
		UpdateShifters();
		switch (index) {
		case 0:
			LoadShifters();
			nextTileId = ppu.Read(0x2000 | (ppu.vramAddress.reg & 0x0FFF));
			break;
		case 2:
			nextTilePaletteId = ppu.Read(0x23C0 | (ppu.vramAddress.nametableY << 11)
				| (ppu.vramAddress.nametableX << 10)
				| ((ppu.vramAddress.coarseY >> 2) << 3)
				| (ppu.vramAddress.coarseX >> 2));

			if (ppu.vramAddress.coarseY & 0x02) {
				nextTilePaletteId >>= 4;
			}
			if (ppu.vramAddress.coarseX & 0x02) {
				nextTilePaletteId >>= 2;
			}
			nextTilePaletteId &= 0x03;
			break;
		case 4:
			nextTileLSBs = ppu.Read((ppu.GetFlag(PatternBackground) << 12)
				+ ((uint16)nextTileId << 4)
				+ (ppu.vramAddress.fineY) + 0);

			break;
		case 6:
			nextTileMSBs = ppu.Read((ppu.GetFlag(PatternBackground) << 12)
				+ ((uint16)nextTileId << 4)
				+ (ppu.vramAddress.fineY) + 8);
			break;
		case 7:
			IncrementScrollX();
		}
	}

	PPU::BackgroundRenderer::CycleResult PPU::BackgroundRenderer::CalculateColor()
	{
		uint8 resultPattern = 0;
		uint8 resultPalette = 0;

		if (ppu.GetFlag(RenderBackground)) {
			uint16 selectedBit = 0x8000 >> ppu.fineX;

			uint8 patternLo = (patternShifterLo & selectedBit) > 0;
			uint8 patternHi = (patternShifterHi & selectedBit) > 0;

			resultPattern = (patternHi << 1) | patternLo;

			uint8_t paletteLo = (attributeShifterLo & selectedBit) > 0;
			uint8_t paletteHi = (attributeShifterHi & selectedBit) > 0;
			resultPalette = (paletteHi << 1) | paletteLo;
		}

		return { ppu.GetPaletteColor(resultPattern, resultPalette), resultPattern == 0 };
	}

	void PPU::BackgroundRenderer::IncrementScrollX()
	{
		if (!ppu.GetFlag(RenderBackground) && !ppu.GetFlag(RenderSprites)) {
			return;
		}

		if (ppu.vramAddress.coarseX == 31) {
			ppu.vramAddress.coarseX = 0;
			ppu.vramAddress.nametableX = ~ppu.vramAddress.nametableX;
		} else {
			++ppu.vramAddress.coarseX;
		}

#ifdef LOG_PPU
		ppu.logger.Log("X++: %04X", ppu.vramAddress.reg);
#endif
	}

	void PPU::BackgroundRenderer::IncrementScrollY()
	{
		if (!ppu.GetFlag(RenderBackground) && !ppu.GetFlag(RenderSprites)) {
			return;
		}

		if (ppu.vramAddress.fineY == 7) {
			ppu.vramAddress.fineY = 0;
			if (ppu.vramAddress.coarseY == 29) {
				ppu.vramAddress.coarseY = 0;
				ppu.vramAddress.nametableY = ~ppu.vramAddress.nametableY;
			} else {
				++ppu.vramAddress.coarseY;
			}
		} else {
			++ppu.vramAddress.fineY;
		}
#ifdef LOG_PPU
		ppu.logger.Log("Y++: %04X", ppu.vramAddress.reg);
#endif
	}

	void PPU::BackgroundRenderer::TransferAddressX()
	{
		if (!ppu.GetFlag(RenderBackground) && !ppu.GetFlag(RenderSprites)) {
			return;
		}

		ppu.vramAddress.nametableX = ppu.tempVramAddress.nametableX;
		ppu.vramAddress.coarseX = ppu.tempVramAddress.coarseX;
	}

	void PPU::BackgroundRenderer::TransferAddressY()
	{
		if (!ppu.GetFlag(RenderBackground) && !ppu.GetFlag(RenderSprites)) {
			return;
		}

		ppu.vramAddress.nametableY = ppu.tempVramAddress.nametableY;
		ppu.vramAddress.coarseY = ppu.tempVramAddress.coarseY;
		ppu.vramAddress.fineY = ppu.tempVramAddress.fineY;
	}

	void PPU::BackgroundRenderer::LoadShifters()
	{
		patternShifterLo = (patternShifterLo & 0xFF00) | nextTileLSBs;
		patternShifterHi = (patternShifterHi & 0xFF00) | nextTileMSBs;

		attributeShifterLo = (attributeShifterLo & 0xFF00) | ((nextTilePaletteId & 0b01) ? 0xFF : 0x00);
		attributeShifterHi = (attributeShifterHi & 0xFF00) | ((nextTilePaletteId & 0b10) ? 0xFF : 0x00);
	}

	void PPU::BackgroundRenderer::UpdateShifters()
	{
		if (!ppu.GetFlag(RenderBackground)) {
			return;
		}

		patternShifterLo <<= 1;
		patternShifterHi <<= 1;
		attributeShifterLo <<= 1;
		attributeShifterHi <<= 1;
	}


	// === ForegroundRenderer ===

	PPU::ForegroundRenderer::ForegroundRenderer(PPU& ppu) : ppu(ppu)
	{
		spritesAtScanline.reserve(8);
	}

	PPU::ForegroundRenderer::CycleResult PPU::ForegroundRenderer::Cycle(uint16 scanline, uint16 cycle)
	{
		if (firstLoadCycle <= cycle && cycle <= lastLoadCycle) {
			UpdateShifters();
		}

		if (cycle == lastLoadCycle && scanline >= firstRenderScanline) {
			EvaluateSprites(scanline);
		}

		if (cycle == lastCycle) {
			FillSpriteBits(scanline);
		}

		return CalculateColor();
	}

	bool PPU::ForegroundRenderer::IsSpriteZeroHit(uint16 cycle)
	{
		const uint16 startingCycle = firstRenderCycle + (ppu.GetFlag(RenderBackgroundLeft) || ppu.GetFlag(RenderSpritesLeft)) ? 0 : 8;
		return isFirstSpriteAtScanline && isFirstSelectedSpriteRendered &&
			ppu.GetFlag(RenderBackground) && ppu.GetFlag(RenderSprites) &&
			startingCycle <= cycle && cycle < 258;
	}

	void PPU::ForegroundRenderer::Reset()
	{
		isFirstSpriteAtScanline = false;
		isFirstSelectedSpriteRendered = false;
		spritesAtScanline.clear();
	}

	PPU::ForegroundRenderer::CycleResult PPU::ForegroundRenderer::CalculateColor()
	{
		if (!ppu.GetFlag(RenderSprites)) {
			return std::nullopt;
		}

		isFirstSelectedSpriteRendered = false;

		for (const SpriteAtScanlineEntry& entry : spritesAtScanline) {
			if (entry.oamEntry.x == 0) {
				const uint8 resultPatternLo = (entry.loBits & 0x80) > 0;
				const uint8 resultPatternHi = (entry.hiBits & 0x80) > 0;
				const uint8 resultPattern = (resultPatternHi << 1) | resultPatternLo;
				const uint8 resultPalette = entry.oamEntry.attribute.palette + 4;

				if (resultPattern != 0) {
					isFirstSelectedSpriteRendered = (&entry == spritesAtScanline.data());
					const Priority priority = entry.oamEntry.attribute.behindBackground ? Priority::Background : Priority::Foreground;
					return std::make_pair(ppu.GetPaletteColor(resultPattern, resultPalette), priority);
				}
			}
		}

		return std::nullopt;
	}

	void PPU::ForegroundRenderer::EvaluateSprites(uint16 scanline)
	{
		spritesAtScanline.clear();
		bool spriteOverflow = false;
		isFirstSpriteAtScanline = false;

		for (uint8 i = 0; i < 64; ++i) {
			const PPU::ObjectAttributeEntry& entry = ppu.objectAttributeMemory[i];
			const int16 diff = (int16)scanline - (int16)entry.y - 1; // -1 to offset scanline indexing starting from 1
			if (0 <= diff && diff < (ppu.GetFlag(LargeSprites) ? 16 : 8)) {
				if (spritesAtScanline.size() < 8) {
					isFirstSpriteAtScanline |= (i == 0);
					spritesAtScanline.push_back({ entry, 0, 0 });
				} else {
					spriteOverflow = true;
					break;
				}
			}
		}

		ppu.SetFlag(SpirteOverflow, spriteOverflow);
	}

	void PPU::ForegroundRenderer::FillSpriteBits(uint16 scanline)
	{
		const uint16 patternTableBaseAddressForSmallSprites = ppu.GetFlag(PatternSprite) << 12;
		for (SpriteAtScanlineEntry& entry : spritesAtScanline) {
			uint16 spriteAddress = 0;
			const uint16 diff = scanline - entry.oamEntry.y - 1; // -1 to offset scanline indexing starting from 1
			if (ppu.GetFlag(LargeSprites)) {
				const uint16 patternTableBaseAddress = (entry.oamEntry.id & 0x01) << 12;
				const bool isInFirstSprite = diff < 8;
				const uint16 spriteBaseAddress = ((entry.oamEntry.id & 0xFE) + (isInFirstSprite ? 0 : 1)) << 4;
				const uint16 lineAddressOffset = entry.oamEntry.attribute.verticalFlip ? 7 - (diff & 0x07) : (diff & 0x07);
				spriteAddress = patternTableBaseAddress + spriteBaseAddress + lineAddressOffset;
			} else {
				const uint16 spriteBaseAddress = entry.oamEntry.id << 4;
				const uint16 lineAddressOffset = (entry.oamEntry.attribute.verticalFlip ? 7 - diff : diff) & 0x07;
				spriteAddress = patternTableBaseAddressForSmallSprites + spriteBaseAddress + lineAddressOffset;
			}
			
			entry.loBits = ppu.Read(spriteAddress);
			entry.hiBits = ppu.Read(spriteAddress + 8);

			if (entry.oamEntry.attribute.horizontalFlip) {
				entry.loBits = MirrorByte(entry.loBits);
				entry.hiBits = MirrorByte(entry.hiBits);
			}
		}
	}

	void PPU::ForegroundRenderer::UpdateShifters()
	{
		if (!ppu.GetFlag(RenderSprites)) {
			return;
		}

		for (SpriteAtScanlineEntry& entry : spritesAtScanline)
		{
			if (entry.oamEntry.x > 0) {
				entry.oamEntry.x--;
			} else {
				entry.loBits <<= 1;
				entry.hiBits <<= 1;
			}
		}
	}


	// PPU Fillers

	void PPU::FillColors()
	{
		colors[0x00] = { 84, 84, 84 };
		colors[0x01] = { 0, 30, 116 };
		colors[0x02] = { 8, 16, 144 };
		colors[0x03] = { 48, 0, 136 };
		colors[0x04] = { 68, 0, 100 };
		colors[0x05] = { 92, 0, 48 };
		colors[0x06] = { 84, 4, 0 };
		colors[0x07] = { 60, 24, 0 };
		colors[0x08] = { 32, 42, 0 };
		colors[0x09] = { 8, 58, 0 };
		colors[0x0A] = { 0, 64, 0 };
		colors[0x0B] = { 0, 60, 0 };
		colors[0x0C] = { 0, 50, 60 };
		colors[0x0D] = { 0, 0, 0 };
		colors[0x0E] = { 0, 0, 0 };
		colors[0x0F] = { 0, 0, 0 };

		colors[0x10] = { 152, 150, 152 };
		colors[0x11] = { 8, 76, 196 };
		colors[0x12] = { 48, 50, 236 };
		colors[0x13] = { 92, 30, 228 };
		colors[0x14] = { 136, 20, 176 };
		colors[0x15] = { 160, 20, 100 };
		colors[0x16] = { 152, 34, 32 };
		colors[0x17] = { 120, 60, 0 };
		colors[0x18] = { 84, 90, 0 };
		colors[0x19] = { 40, 114, 0 };
		colors[0x1A] = { 8, 124, 0 };
		colors[0x1B] = { 0, 118, 40 };
		colors[0x1C] = { 0, 102, 120 };
		colors[0x1D] = { 0, 0, 0 };
		colors[0x1E] = { 0, 0, 0 };
		colors[0x1F] = { 0, 0, 0 };

		colors[0x20] = { 236, 238, 236 };
		colors[0x21] = { 76, 154, 236 };
		colors[0x22] = { 120, 124, 236 };
		colors[0x23] = { 176, 98, 236 };
		colors[0x24] = { 228, 84, 236 };
		colors[0x25] = { 236, 88, 180 };
		colors[0x26] = { 236, 106, 100 };
		colors[0x27] = { 212, 136, 32 };
		colors[0x28] = { 160, 170, 0 };
		colors[0x29] = { 116, 196, 0 };
		colors[0x2A] = { 76, 208, 32 };
		colors[0x2B] = { 56, 204, 108 };
		colors[0x2C] = { 56, 180, 204 };
		colors[0x2D] = { 60, 60, 60 };
		colors[0x2E] = { 0, 0, 0 };
		colors[0x2F] = { 0, 0, 0 };

		colors[0x30] = { 236, 238, 236 };
		colors[0x31] = { 168, 204, 236 };
		colors[0x32] = { 188, 188, 236 };
		colors[0x33] = { 212, 178, 236 };
		colors[0x34] = { 236, 174, 236 };
		colors[0x35] = { 236, 174, 212 };
		colors[0x36] = { 236, 180, 176 };
		colors[0x37] = { 228, 196, 144 };
		colors[0x38] = { 204, 210, 120 };
		colors[0x39] = { 180, 222, 120 };
		colors[0x3A] = { 168, 226, 144 };
		colors[0x3B] = { 152, 226, 180 };
		colors[0x3C] = { 160, 214, 228 };
		colors[0x3D] = { 160, 162, 160 };
		colors[0x3E] = { 0, 0, 0 };
		colors[0x3F] = { 0, 0, 0 };
	}

}
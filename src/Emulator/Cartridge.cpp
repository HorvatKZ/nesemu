#include "Cartridge.hpp"
#include "Cartridge.hpp"
#include "Cartridge.hpp"
#include "Mappers/Mapper0.hpp"
#include "Mappers/Mapper1.hpp"
#include "Mappers/Mapper2.hpp"
#include "Mappers/Mapper3.hpp"
#include "Mappers/Mapper66.hpp"

#include <fstream>
#include <functional>


namespace nes {

	Cartridge::MapperReadResultVisitor::MapperReadResultVisitor(const std::vector<uint8>& memory) : memory(memory)
	{
	}

	uint8 Cartridge::MapperReadResultVisitor::operator()(const Mapper::MappedAddress& mappedAddress) const
	{
		if (mappedAddress.address < memory.size()) {
			return memory[mappedAddress.address];
		}
		return 0;
	}

	uint8 Cartridge::MapperReadResultVisitor::operator()(const Mapper::DataFromRAM& dataFromRAM) const
	{
		return dataFromRAM.data;
	}

	uint8 Cartridge::MapperReadResultVisitor::operator()(const Mapper::EmptyResult&) const
	{
		return 0;
	}


	Cartridge::MapperWriteResultVisitor::MapperWriteResultVisitor(std::vector<uint8>& memory) : memory(memory)
	{
	}

	void Cartridge::MapperWriteResultVisitor::operator()(const Mapper::MappedAddress& mappedAddress) const
	{
		if (mappedAddress.address < memory.size()) {
			memory[mappedAddress.address] = value;
		}
	}

	void Cartridge::MapperWriteResultVisitor::operator()(const Mapper::DataFromRAM& dataFromRAM) const
	{
	}

	void Cartridge::MapperWriteResultVisitor::operator()(const Mapper::EmptyResult&) const
	{
	}


#ifdef LOG_MAPPER
	Cartridge::MapperLoggerVisitor::MapperLoggerVisitor() : logger("mapperdump.txt")
	{
	}

	const char* Cartridge::MapperLoggerVisitor::OperationToStr() const
	{
		switch (operation) {
			case Operation::CPURead: return "CPUR";
			case Operation::CPUWrite: return "CPUW";
			case Operation::PPURead: return "PPUR";
			case Operation::PPUWrite: return "PPUW";
			default: return "XXXX";
		}
	}

	void Cartridge::MapperLoggerVisitor::operator()(const Mapper::MappedAddress& mappedAddress)
	{
		logger.Log("%s: %04X -> %08X", OperationToStr(), address, mappedAddress.address);
	}

	void Cartridge::MapperLoggerVisitor::operator()(const Mapper::DataFromRAM& dataFromRAM)
	{
		logger.Log("%s: %04X == %02X", OperationToStr(), address, dataFromRAM.data);
	}

	void Cartridge::MapperLoggerVisitor::operator()(const Mapper::EmptyResult&)
	{
		logger.Log("%s: %04X {}", OperationToStr(), address);
	}

	void Cartridge::LogMapper(MapperLoggerVisitor::Operation operation, uint16 address, const Mapper::Result& result) const
	{
		loggerVisitor.operation = operation;
		loggerVisitor.address = address;
		std::visit(loggerVisitor, result);
	}
#endif


	Cartridge::Cartridge(const std::string& fileName) : cpuReadVisitor(programMemory), cpuWriteVisitor(programMemory), ppuReadVisitor(characterMemory), ppuWriteVisitor(characterMemory)
	{
		Load(fileName);
	}


	uint8 Cartridge::CPURead(uint16 address) const
	{
		if (mapper == nullptr) {
			return 0;
		}

		const Mapper::Result mapperResult = mapper->MapCPUAddressForRead(address);

#ifdef LOG_MAPPER
		LogMapper(MapperLoggerVisitor::Operation::CPURead, address, mapperResult);
#endif
		return std::visit(cpuReadVisitor, mapperResult);
	}


	void Cartridge::CPUWrite(uint16 address, uint8 value)
	{
		if (mapper == nullptr) {
			return;
		}

		const Mapper::Result mapperResult = mapper->MapCPUAddressForWrite(address, value);

#ifdef LOG_MAPPER
		LogMapper(MapperLoggerVisitor::Operation::CPUWrite, address, mapperResult);
#endif
		cpuWriteVisitor.value = value;
		return std::visit(cpuWriteVisitor, mapperResult);
	}


	uint8 Cartridge::PPURead(uint16 address) const
	{
		if (mapper == nullptr) {
			return 0;
		}

		const Mapper::Result mapperResult = mapper->MapPPUAddressForRead(address);
		
#ifdef LOG_MAPPER
		LogMapper(MapperLoggerVisitor::Operation::PPURead, address, mapperResult);
#endif
		return std::visit(ppuReadVisitor, mapperResult);
	}


	void Cartridge::PPUWrite(uint16 address, uint8 value)
	{
		if (mapper == nullptr) {
			return;
		}

		const Mapper::Result mapperResult = mapper->MapPPUAddressForWrite(address);

#ifdef LOG_MAPPER
		LogMapper(MapperLoggerVisitor::Operation::PPUWrite, address, mapperResult);
#endif
		ppuWriteVisitor.value = value;
		return std::visit(ppuWriteVisitor, mapperResult);
	}


	MirroringMode Cartridge::GetMirroring() const
	{
		if (mapper == nullptr) {
			return hardwareMirroring;
		}

		const MirroringMode mapperMirroring = mapper->GetMirroring();
		if (mapperMirroring == MirroringMode::HARDWARE_DETEMINED) {
			return hardwareMirroring;
		}

		return mapperMirroring;
	}


	void Cartridge::Reset()
	{
		if (mapper != nullptr) {
			mapper->Reset();
		}
	}


	void Cartridge::Load(const std::string& fileName)
	{
		std::ifstream fin;
		fin.open(fileName, std::ifstream::binary);
		if (!fin.is_open()) {
			return;
		}

		Header header;
		fin.read(reinterpret_cast<char*>(&header), sizeof(Header));

		if (header.mapper1 & 0x04) {
			fin.seekg(512, std::ios_base::cur);
		}

		mapperID = ((header.mapper2 >> 4) << 4) | (header.mapper1 >> 4);
		hardwareMirroring = static_cast<MirroringMode> (header.mapper1 & 0x01);

		programROMChunks = header.programROMChunks;
		programMemory.resize(programROMChunks * 16384);
		fin.read(reinterpret_cast<char*>(programMemory.data()), programMemory.size());

		characterROMChunks = header.characterROMChunks;
		if (characterROMChunks == 0) {
			characterMemory.resize(8192);
		} else {
			characterMemory.resize(characterROMChunks * 8192);
		}
		fin.read(reinterpret_cast<char*>(characterMemory.data()), characterMemory.size());

		fin.close();

		mapper = std::unique_ptr<Mapper>(CreateMapper());
	}


	Mapper* Cartridge::CreateMapper()
	{
		switch (mapperID) {
			case 0: return new Mapper0(programROMChunks);
			case 1: return new Mapper1(programROMChunks, characterROMChunks == 0);
			case 2: return new Mapper2(programROMChunks);
			case 3: return new Mapper3(programROMChunks);
			case 66: return new Mapper66();
			default: return nullptr;
		}
	}
}
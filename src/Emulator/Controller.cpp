#include "Controller.hpp"


namespace nes {

	void Controller::Update(Key key, bool isPressed)
	{
		if (isPressed) {
			state |= key;
		} else {
			state &= ~key;
		}
	}


	bool Controller::IsPressed(Key key)
	{
		return state & key;
	}


	void Controller::Store()
	{
		readBuffer = state;
	}


	uint8 Controller::Read()
	{
		uint8 nextBit = (readBuffer & 0x80) >> 7;
		readBuffer <<= 1;
		return nextBit;
	}
}
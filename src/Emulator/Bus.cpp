#include "Bus.hpp"


namespace nes {

	Bus::Bus(uint32 audioSampleRate) : cpu(*this), ppu(*this), dma(*this), apu(audioSampleRate)
	{
		memset(ram, 0, sizeof(ram));
	}


	Bus::~Bus()
	{
	}


	uint8 Bus::Read(uint16 address) const
	{
		if (address < 0x2000) {
			return ram[address & 0x07FF];
		} else if (address < 0x4000) {
			return const_cast<PPU&>(ppu).CPURead(address & 0x0007);
		} else if ((0x4000 <= address && address <= 0x4013) || address == 0x4015) {
			return apu.CPURead(address);
		} else if (0x4016 <= address && address <= 0x4017) {
			return const_cast<Controller&>(controllers[address & 0x0001]).Read();
		} else if (0x4020 <= address && cartridge != nullptr) {
			return cartridge->CPURead(address);
		}

		return 0;
	}


	void Bus::Write(uint16 address, uint8 value)
	{
		if (address < 0x2000) {
			ram[address & 0x07FF] = value;
		} else if (address < 0x4000) {
			ppu.CPUWrite(address & 0x0007, value);
		} else if ((0x4000 <= address && address <= 0x4013) || address == 0x4015) {
			apu.CPUWrite(address, value);
		} else if (address == 0x4014) {
			dma.StartTransfer(value);
		} else if (0x4016 <= address && address <= 0x4017) {
			controllers[address & 0x0001].Store();
		} else if (0x4020 <= address && cartridge != nullptr) {
			cartridge->CPUWrite(address, value);
		}
	}


	void Bus::LoadCartridge(const std::string& fileName)
	{
		cartridge = std::make_shared<Cartridge>(fileName);
		ppu.ConnectCartridge(cartridge);
	}


	void Bus::Reset()
	{
		cpu.Reset();
		ppu.Reset();
		if (cartridge != nullptr) {
			cartridge->Reset();
		}
		dma.Reset();
		apu.Reset();

		clockCount = 0;
	}


	void Bus::Clock()
	{
		ppu.Clock();
		apu.Clock();
		if (clockCount % 3 == 0) {
			if (dma.IsTransferring()) {
				dma.Clock(clockCount);
			} else {
				cpu.Clock();
			}
		}

		if (ppu.EmitNonMaskableInterrupt()) {
			cpu.NonMaskableInterrupt();
		}

		++clockCount;
	}


	void Bus::UpdateControllerKey(Controller::Key key, bool isPressed)
	{
		controllers[0].Update(key, isPressed);
		controllers[1].Update(key, isPressed);
	}


	bool Bus::IsControllerKeyPressed(uint8 index, Controller::Key key)
	{
		return controllers[index].IsPressed(key);
	}


	Bus::DirectMemoryAccessor::DirectMemoryAccessor(Bus& bus) : bus(bus)
	{
	}


	void Bus::DirectMemoryAccessor::StartTransfer(uint8 pageIndex)
	{
		this->pageIndex = pageIndex;
		pageAddress = 0;
		isTransferring = true;
		shouldWaitForClockSync = true;
	}


	void Bus::DirectMemoryAccessor::Clock(uint32 clockCount)
	{
		if (shouldWaitForClockSync) {
			if (clockCount % 2 == 1) {
				shouldWaitForClockSync = false;
			}
			return;
		}

		if (clockCount % 2 == 0) {
			dataBuffer = bus.Read(pageIndex << 8 | pageAddress);
		} else {
			bus.GetPPU().WriteToOAM(pageAddress, dataBuffer);

			if (pageAddress == 255) {
				isTransferring = false;
			} else {
				++pageAddress;
			}
		}
	}


	bool Bus::DirectMemoryAccessor::IsTransferring()
	{
		return isTransferring;
	}


	void Bus::DirectMemoryAccessor::Reset()
	{
		pageIndex = 0;
		pageAddress = 0;
		dataBuffer = 0;
		isTransferring = false;
		shouldWaitForClockSync = true;
	}

}
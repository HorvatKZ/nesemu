#include "APU.hpp"

#include <cmath>


namespace nes {

	static const float PI = 3.14159f;
	static const uint8 squareWaveHarmonics = 20;
	static const uint8 triangleWaveHarmonics = 10;
	static uint8_t lengthCounterLookup[] = {
		10, 254, 20,  2, 40,  4, 80,  6, 160,   8, 60, 10, 14, 12, 26, 14,
		12,  16, 24, 18, 48, 20, 96, 22, 192,  24, 72, 26, 16, 28, 32, 30
	};
	static uint8_t noiseReloadLookup[] = {
		0, 4, 8, 16, 32, 64, 96, 128, 160, 202, 254, 380, 508, 1016, 2034, 4068
	};


	static float fastSin (float t)
	{
		float j = t * 0.15915f;
		j = j - (uint32)j;
		return 20.785f * j * (j - 0.5f) * (j - 1.0f);
	}


	static uint8 RotateByte(uint8 data) {
		return ((data & 0x01) << 7) | ((data & 0xFE) >> 1);
	}


	static uint16 NoiseBitShuffle(uint16 data) {
		return (((data & 0x0001) ^ ((data & 0x0002) >> 1)) << 14) | ((data & 0x7FFF) >> 1);
	}


	APU::APU(uint32 sampleRate) : sampleTime(1.0 / sampleRate), noise1(*this)
	{
	}


	void APU::Clock()
	{
		DoRealTimeSampling();

		if (clockCount % 6 == 0) {
			++frameClockCount;
			const bool isQuarterFrame = (frameClockCount == 3729 || frameClockCount == 7454 ||
				frameClockCount == 11186 || frameClockCount == 14916);
			const bool isHalfFrame = (frameClockCount == 7454 || frameClockCount == 14916);
			if (frameClockCount == 14916) {
				frameClockCount = 0;
			}

			if (isQuarterFrame) {
				pulse1.ClockEnvelope();
				pulse2.ClockEnvelope();
				noise1.ClockEnvelope();
			}

			if (isHalfFrame) {
				pulse1.LengthCounterClock();
				pulse2.LengthCounterClock();
				noise1.LengthCounterClock();

				pulse1.SweeperClock(Sweeper::OnesComplement);
				pulse2.SweeperClock(Sweeper::TwosComplement);
			}

			pulse1.Clock();
			pulse2.Clock();
			triangle1.Clock();
			noise1.Clock();
		}

		++clockCount;
	}


	void APU::DoRealTimeSampling()
	{
		justFinishedSample = false;
		globalTime += clockTime;
		nextSampleTime += clockTime;

		if (nextSampleTime >= sampleTime) {
			nextSampleTime -= sampleTime;
			lastSample = Sample();
			justFinishedSample = true;
		}
	}


	uint8 APU::CPURead(uint16 address) const
	{
		return 0;
	}


	void APU::CPUWrite(uint16 address, uint8 value)
	{
		if (0x4000 <= address && address < 0x4004) {
			pulse1.Write(address & 0x03, value);
		} else if (0x4004 <= address && address < 0x4008) {
			pulse2.Write(address & 0x03, value);
		} else if (0x4008 <= address && address < 0x400C) {
			triangle1.Write(address & 0x03, value);
		} else if (0x400C <= address && address < 0x4010) {
			noise1.Write(address & 0x03, value);
		} else if (0x4015 == address) {
			pulse1.SetEnabled(value & 0x01);
			pulse2.SetEnabled(value & 0x02);
			triangle1.SetEnabled(value & 0x04);
			noise1.SetEnabled(value & 0x08);
		}
	}


	void APU::Reset()
	{
	}


	float APU::Sample()
	{
		return (pulse1.Sample(globalTime) + pulse2.Sample(globalTime) + triangle1.Sample(globalTime) + noise1.Sample(globalTime)) * 0.25f;
	}


	void APU::Envelope::Clock(bool shouldLoop)
	{
		if (hasStartSignal) {
			hasStartSignal = false;
			decayCount = 15;
			deviderCount = volume;
			return;
		}

		if (deviderCount > 0) {
			--deviderCount;
			return;
		}

		deviderCount = volume;
		if (decayCount > 0) {
			--decayCount;
			return;
		}

		if (shouldLoop) {
			decayCount = 15;
		}
	}


	uint8 APU::Envelope::GetOutput() const {
		return useConstantVolume ? volume : decayCount;
	}


	void APU::Sweeper::Clock(uint16& target, CarryType carryType)
	{
		const uint16 changeAmount = target >> shift;
		if (isEnabled && timer == 0 && shift > 0 && !isMuting && target >= 8 && changeAmount < 0x07FF) {
			switch (mode) {
				case Add:
					target += changeAmount;
					break;
				case Substract:
					switch (carryType) {
						case OnesComplement:
							target -= changeAmount;
							break;
						case TwosComplement:
							target -= changeAmount + 1;
							break;
					}
					break;
			}
		}

		if (timer == 0 || hasReloadSignal) {
			timer = period;
			hasReloadSignal = false;
		} else {
			--timer;
		}

		isMuting = (target < 8) || (target > 0x7FF);
	}


	void APU::Sweeper::Write(uint8 value)
	{
		isEnabled = (value & 0x80);
		period = (value & 0x70) >> 4;
		mode = (value & 0x08) ? Substract : Add;
		shift = (value & 0x07);
		hasReloadSignal = true;
	}


	void APU::PulseChannel::Clock()
	{
		if (!isEnabled) {
			return;
		}

		if (sequencer.timer == 0) {
			sequencer.timer = sequencer.reloadValue;
			sequencer.sequence = RotateByte(sequencer.sequence);
			output = sequencer.sequence & 0x01;
		} else {
			--sequencer.timer;
		}
	}


	void APU::PulseChannel::LengthCounterClock()
	{
		if (!isEnabled) {
			lengthCounter = 0;
			return;
		}

		if (!lengthCounterHalt && lengthCounter > 0) {
			--lengthCounter;
		}
	}


	void APU::PulseChannel::SweeperClock(Sweeper::CarryType carryType)
	{
		sweeper.Clock(sequencer.reloadValue, carryType);
	}


	void APU::PulseChannel::ClockEnvelope()
	{
		envelope.Clock(lengthCounterHalt);
	}


	bool APU::PulseChannel::IsSilenced() const
	{
		return forceDisable || !isEnabled || lengthCounter == 0 || sequencer.timer < 8 || envelope.GetOutput() <= 2 || sweeper.isMuting;
	}


	float APU::PulseChannel::Sample(float globalTime)
	{
		if (IsSilenced()) {
			return 0.0f;
		}

		const float frequency = 1789773.f / (16.f * (float)(sequencer.reloadValue + 1));
		float a = 0;
		float b = 0;
		float p = dutyCycle * 2.0 * PI;

		for (uint8 n = 1; n < squareWaveHarmonics; ++n) {
			float c = n * frequency * 2.0 * PI * globalTime;
			a += -fastSin(c) / n;
			b += -fastSin(c - p * n) / n;
		}

		const float amplitude = (envelope.GetOutput() - 1) / 16.0f;
		return 2.0 * amplitude / PI * (a - b);
	}


	void APU::PulseChannel::Write(uint8 subAddress, uint8 value)
	{
		switch (subAddress) {
			case 0x00:
				switch ((value & 0xC0) >> 6) {
					case 0x00:
						sequencer.sequence = 0b01000000;
						dutyCycle = 0.125f;
						break;
					case 0x01:
						sequencer.sequence = 0b01100000;
						dutyCycle = 0.25f;
						break;
					case 0x02:
						sequencer.sequence = 0b01111000;
						dutyCycle = 0.5f;
						break;
					case 0x03:
						sequencer.sequence = 0b10011111;
						dutyCycle = 0.75f;
						break;
				}
				lengthCounterHalt = (value & 0x20);
				envelope.useConstantVolume = (value & 0x10);
				envelope.volume = (value & 0x0F);
				break;
			case 0x01:
				sweeper.Write(value);
				break;
			case 0x02:
				sequencer.reloadValue = (sequencer.reloadValue & 0xFF00) | value;
				break;
			case 0x03:
				sequencer.reloadValue = (uint16)((value & 0x07)) << 8 | (sequencer.reloadValue & 0x00FF);
				sequencer.timer = sequencer.reloadValue;
				lengthCounter = lengthCounterLookup[(value & 0xF8) >> 3];
				envelope.hasStartSignal = true;
				break;
		}
	}


	void APU::PulseChannel::SetEnabled(bool isEnabled)
	{
		this->isEnabled = isEnabled;
	}


	void APU::PulseChannel::SetForceDisable(bool forceDisable)
	{
		this->forceDisable = forceDisable;
	}


	void APU::PulseChannel::SetEnvelopeStartSignal()
	{
		envelope.hasStartSignal = true;
	}


	void APU::TriangleChannel::Clock()
	{
		if (!isEnabled) {
			return;
		}

		if (sequencer.timer == 0) {
			sequencer.timer = sequencer.reloadValue;
			//sequencer.sequence = RotateByte(sequencer.sequence);
			output = sequencer.sequence & 0x01;
		} else {
			--sequencer.timer;
		}
	}


	void APU::TriangleChannel::LengthCounterClock()
	{
		if (!isEnabled) {
			lengthCounter = 0;
			return;
		}

		if (!lengthCounterHalt && lengthCounter > 0) {
			--lengthCounter;
		}
	}


	bool APU::TriangleChannel::IsSilenced() const
	{
		return forceDisable || !isEnabled || sequencer.timer < 8;
	}


	float APU::TriangleChannel::Sample(float globalTime)
	{
		if (IsSilenced()) {
			return 0.0f;
		}

		const float frequency = 1789773.f / (32.f * (float)(sequencer.reloadValue + 1));
		float sum = 0.0f;
		float sign = 1.0f;
		for (uint8 i = 0; i < triangleWaveHarmonics; ++i, sign *= -1) {
			const uint8 n = 2 * i + 1;
			const float c = n * frequency * 2.0f * PI * globalTime;
			sum += sign * fastSin(c) / (n * n);
		}

		return sum * 8.0f / (PI * PI);
	}


	void APU::TriangleChannel::Write(uint8 subAddress, uint8 value)
	{
		switch (subAddress) {
			case 0x00:
				lengthCounterHalt = (value & 0x80);
				break;
			case 0x01:
				break;
			case 0x02:
				sequencer.reloadValue = (sequencer.reloadValue & 0xFF00) | value;
				break;
			case 0x03:
				sequencer.reloadValue = (uint16)((value & 0x07)) << 8 | (sequencer.reloadValue & 0x00FF);
				sequencer.timer = sequencer.reloadValue;
				lengthCounter = lengthCounterLookup[(value & 0xF8) >> 3];
				break;
		}
	}


	void APU::TriangleChannel::SetEnabled(bool isEnabled)
	{
		this->isEnabled = isEnabled;
	}


	void APU::TriangleChannel::SetForceDisable(bool forceDisable)
	{
		this->forceDisable = forceDisable;
	}


	APU::NoiseChannel::NoiseChannel(APU& apu) : apu(apu)
	{
		sequencer.sequence = 0xDBDB;
	}


	void APU::NoiseChannel::Clock()
	{
		if (!isEnabled) {
			return;
		}

		if (sequencer.timer == 0) {
			sequencer.timer = sequencer.reloadValue;
			sequencer.sequence = NoiseBitShuffle(sequencer.sequence);
			output = sequencer.sequence & 0x01;
		}
		else {
			--sequencer.timer;
		}
	}


	void APU::NoiseChannel::LengthCounterClock()
	{
		if (!isEnabled) {
			lengthCounter = 0;
			return;
		}

		if (!lengthCounterHalt && lengthCounter > 0) {
			--lengthCounter;
		}
	}


	void APU::NoiseChannel::ClockEnvelope()
	{
		envelope.Clock(lengthCounterHalt);
	}


	bool APU::NoiseChannel::IsSilenced() const
	{
		return forceDisable || !isEnabled || lengthCounter == 0 || sequencer.timer < 8 || envelope.GetOutput() <= 2;
	}


	float APU::NoiseChannel::Sample(float globalTime)
	{
		if (IsSilenced()) {
			return 0.0f;
		}

		return (float)output * (float)envelope.GetOutput() / 16.0f;
	}


	void APU::NoiseChannel::Write(uint8 subAddress, uint8 value)
	{
		switch (subAddress) {
			case 0x00:
				lengthCounterHalt = (value & 0x20);
				envelope.useConstantVolume = (value & 0x10);
				envelope.volume = (value & 0x0F);
				break;
			case 0x01:
				break;
			case 0x02:
				sequencer.reloadValue = noiseReloadLookup[value & 0x0F];
				break;
			case 0x03:
				lengthCounter = lengthCounterLookup[(value & 0xF8) >> 3];
				envelope.hasStartSignal = true;
				apu.pulse1.SetEnvelopeStartSignal();
				apu.pulse2.SetEnvelopeStartSignal();
				break;
		}
	}


	void APU::NoiseChannel::SetEnabled(bool isEnabled)
	{
		this->isEnabled = isEnabled;
	}


	void APU::NoiseChannel::SetForceDisable(bool forceDisable)
	{
		this->forceDisable = forceDisable;
	}

}
#pragma once

#include "defines.h"
#include "Cartridge.hpp"

#include <optional>

#ifdef LOG_PPU
	#include "Logger.hpp"
#endif


namespace nes {

	class Bus;

	class PPU
	{
		friend class DebugView;

	public:
		struct Color {
			uint8 r, g, b;
		};

	public:
		PPU(Bus& bus);

		void Clock();

		uint8 CPURead(uint16 address);
		void CPUWrite(uint16 address, uint8 value);

		void WriteToOAM(uint8 address, uint8 value);

		void ConnectCartridge(const std::shared_ptr<Cartridge>& cartridge);

		std::vector<Color> GetPatternTable(uint8 index, uint8 palette) const;
		std::vector<Color> GetPalette(uint8 index) const;

		bool EmitNonMaskableInterrupt();
		bool IsWritingToScreenBuffer();
		bool IsFrameReady();

		void Reset();

		inline const std::vector<Color> GetScreenBuffer() const { return screenBuffer; }

	private: // Types
		enum class Interfaces {
			Control = 0,
			Mask = 1,
			Status = 2,
			OAM_Address = 3,
			OAM_Data = 4,
			Scroll = 5,
			Address = 6,
			Data = 7
		};

		enum StatusFlag {
			SpirteOverflow = (1 << 5),
			SpirteZeroHit = (1 << 6),
			VerticalBlank = (1 << 7)
		};

		enum MaskFlag {
			Grayscale = (1 << 0),
			RenderBackgroundLeft = (1 << 1),
			RenderSpritesLeft = (1 << 2),
			RenderBackground = (1 << 3),
			RenderSprites = (1 << 4),
			EnhanceRed = (1 << 5),
			EnhanceGreen = (1 << 6),
			EnhanceBlue = (1 << 7)
		};

		enum ControlFlag {
			NameTableX = (1 << 0),
			NameTableY = (1 << 1),
			IncrementMode = (1 << 2),
			PatternSprite = (1 << 3),
			PatternBackground = (1 << 4),
			LargeSprites = (1 << 5),
			SlaveMode = (1 << 6),
			EnableNonMaskableInterrupt = (1 << 7)
		};

		union LoopyRegister {
			struct {
				uint16 coarseX : 5;
				uint16 coarseY : 5;
				uint16 nametableX : 1;
				uint16 nametableY : 1;
				uint16 fineY : 3;
				uint16 unused : 1;
			};

			uint16 reg = 0x0000;
		};

		struct OAMAttribute {
			uint8 palette : 2;
			uint8 unused : 3;
			uint8 behindBackground : 1;
			uint8 horizontalFlip : 1;
			uint8 verticalFlip : 1;
		};

		struct ObjectAttributeEntry {
			uint8 y;
			uint8 id;
			OAMAttribute attribute;
			uint8 x;
		};

		class BackgroundRenderer {
		public:
			struct CycleResult {
				Color color;
				bool isBackgroundColor;
			};

			BackgroundRenderer(PPU& ppu);

			CycleResult Cycle(uint16 scanline, uint16 cycle);

			void Reset();

		private:
			PPU& ppu;

			uint8 nextTileId = 0;
			uint8 nextTilePaletteId = 0;
			uint8 nextTileLSBs = 0, nextTileMSBs = 0;

			uint16 patternShifterLo = 0, patternShifterHi = 0;
			uint16 attributeShifterLo = 0, attributeShifterHi = 0;

			void LoadCycle(uint8 index);

			CycleResult CalculateColor();

			void IncrementScrollX();
			void IncrementScrollY();
			void TransferAddressX();
			void TransferAddressY();
			void LoadShifters();
			void UpdateShifters();
		};

		class ForegroundRenderer {
		public:
			enum Priority {
				Foreground,
				Background
			};

			using CycleResult = std::optional<std::pair<Color, Priority>>;

			ForegroundRenderer(PPU& ppu);

			CycleResult Cycle(uint16 scanline, uint16 cycle);

			bool IsSpriteZeroHit(uint16 cycle);

			void Reset();

		private:
			struct SpriteAtScanlineEntry {
				ObjectAttributeEntry oamEntry;
				uint8 loBits, hiBits;
			};

			PPU& ppu;

			std::vector<SpriteAtScanlineEntry> spritesAtScanline;
			bool isFirstSpriteAtScanline = false, isFirstSelectedSpriteRendered = false;

			CycleResult CalculateColor();

			void EvaluateSprites(uint16 scanline);
			void FillSpriteBits(uint16 scanline);
			void UpdateShifters();
		};

	private: // Members
		Bus& bus;
		std::shared_ptr<Cartridge> cartridge;
		BackgroundRenderer bgRenderer;
		ForegroundRenderer fgRenderer;
		uint8 status = 0, mask = 0, control = 0;

		uint8 nameTables[2][1024], paletteTable[32];

		ObjectAttributeEntry objectAttributeMemory[64];
		uint8* oamByteAccessor;

		std::vector<Color> screenBuffer;
		std::vector<Color> colors;
		bool frameJustCompleted = false;
		uint16 cycle = 0, scanline = 1;
		uint32 frameCount = 0, clockCount = 0;
		bool emitNonMaskableInterrupt = false;

		bool dataAddressHiByte = true;
		uint8 dataBuffer = 0;
		LoopyRegister vramAddress, tempVramAddress;
		uint8 fineX = 0;
		uint8 oamAddress = 0;

#ifdef LOG_PPU
		Logger logger;
#endif

	private: // Methods
		uint8 Read(uint16 address) const;
		void Write(uint16 address, uint8 value);

		bool GetFlag(StatusFlag flag) const;
		bool GetFlag(MaskFlag flag) const;
		bool GetFlag(ControlFlag flag) const;
		void SetFlag(StatusFlag flag, bool value);
		void SetFlag(MaskFlag flag, bool value);
		void SetFlag(ControlFlag flag, bool value);

		Color GeretatePixel(uint16 x, uint16 y);
		Color GetPaletteColor(uint8 pixelValue, uint8 palette) const;
		uint8 GetPaletteColorID(uint8 pixelValue, uint8 palette) const;

		Color MergeColors(const BackgroundRenderer::CycleResult& bg, const ForegroundRenderer::CycleResult& fg);

		void FillColors();
	};
}
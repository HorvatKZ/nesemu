#pragma once

#include "defines.h"


namespace nes {

	class APU
	{
		friend class DebugView;

	public:
		APU(uint32 sampleRate);

		void Clock();

		uint8 CPURead(uint16 address) const;
		void CPUWrite(uint16 address, uint8 value);

		void Reset();

		inline float GetSample() { return lastSample; }
		inline bool JustFinishedSample() { return justFinishedSample; }
	
	private:
		struct Sequencer {
			uint32 sequence = 0;
			uint16 timer = 0;
			uint16 reloadValue = 0;
		};

		struct Envelope {
			bool hasStartSignal = false;
			bool useConstantVolume = false;
			uint8 volume = 0;
			uint8 deviderCount = 0;
			uint8 decayCount = 0;

			void Clock(bool shouldLoop);
			uint8 GetOutput() const;
		};

		struct Sweeper {
			enum Mode {
				Add,
				Substract
			};

			enum CarryType {
				OnesComplement,
				TwosComplement
			};

			bool isEnabled = false;
			bool isMuting = false;
			bool hasReloadSignal = false;
			uint8 period = 0;
			uint8 shift = 0;
			uint8 timer = 0;
			Mode mode = Add;

			void Clock(uint16& target, CarryType carryType);
			void Write(uint8 value);
		};

		class PulseChannel {
		public:
			void Clock();
			void LengthCounterClock();
			void SweeperClock(Sweeper::CarryType carryType);
			void ClockEnvelope();

			float Sample(float globalTime);
			void Write(uint8 subAddress, uint8 value);
			void SetEnabled(bool isEnabled);

			void SetForceDisable(bool forceDisable);

			void SetEnvelopeStartSignal();

		private:
			Sequencer sequencer;
			Envelope envelope;
			Sweeper sweeper;
			bool isEnabled = false;
			bool forceDisable = false;
			uint8 output = 0;
			uint8 lengthCounter = 0;
			bool lengthCounterHalt = false;

			float dutyCycle = 0.5;

			bool IsSilenced() const;
		};

		class TriangleChannel {
		public:
			void Clock();
			void LengthCounterClock();

			float Sample(float globalTime);
			void Write(uint8 subAddress, uint8 value);
			void SetEnabled(bool isEnabled);

			void SetForceDisable(bool forceDisable);

		private:
			Sequencer sequencer;
			bool isEnabled = false;
			bool forceDisable = false;
			uint8 output = 0;
			uint8 lengthCounter = 0;
			bool lengthCounterHalt = false;

			bool IsSilenced() const;
		};

		class NoiseChannel {
		public:
			NoiseChannel(APU& apu);

			void Clock();
			void LengthCounterClock();
			void ClockEnvelope();

			float Sample(float globalTime);
			void Write(uint8 subAddress, uint8 value);
			void SetEnabled(bool isEnabled);

			void SetForceDisable(bool forceDisable);

		private:
			APU& apu;

			Sequencer sequencer;
			Envelope envelope;
			bool isEnabled = false;
			bool forceDisable = false;
			uint8 lengthCounter = 0;
			uint8 output = 0;
			bool lengthCounterHalt = false;

			bool IsSilenced() const;
		};

		const double sampleTime;
		const double clockTime = 1.0 / 5369318.0;

		double globalTime = 0.0;
		double nextSampleTime = 0.0;
		float lastSample = 0.f;
		bool justFinishedSample = false;

		uint32 clockCount = 0;
		uint32 frameClockCount = 0;

		PulseChannel pulse1;
		PulseChannel pulse2;
		TriangleChannel triangle1;
		NoiseChannel noise1;

		void DoRealTimeSampling();
		float Sample();
	};

}
#pragma once

#include "defines.h"


namespace nes {

	class Controller
	{
	public:
		enum Key {
			Right = (1 << 0),
			Left = (1 << 1),
			Down = (1 << 2),
			Up = (1 << 3),
			Start = (1 << 4),
			Select = (1 << 5),
			B = (1 << 6),
			A = (1 << 7)
		};

		void Update(Key key, bool isPressed);
		bool IsPressed(Key key);
		
		void Store();
		uint8 Read();

	private:
		uint8 state = 0;
		uint8 readBuffer = 0;
	};

}
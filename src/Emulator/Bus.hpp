#pragma once

#include "defines.h"
#include "CPU.hpp"
#include "PPU.hpp"
#include "APU.hpp"
#include "Cartridge.hpp"
#include "Controller.hpp"


namespace nes {

	class Bus
	{
	public:
		Bus(uint32 audioSampleRate);
		~Bus();

		uint8 Read(uint16 address) const;
		void Write(uint16 address, uint8 value);

		void LoadCartridge(const std::string& fileName);

		void Reset();
		void Clock();

		void UpdateControllerKey(Controller::Key key, bool isPressed);
		bool IsControllerKeyPressed(uint8 index, Controller::Key key);

		inline CPU& GetCPU() { return cpu; }
		inline PPU& GetPPU() { return ppu; }
		inline APU& GetAPU() { return apu; }

	private:
		class DirectMemoryAccessor {
		public:
			DirectMemoryAccessor(Bus& bus);

			void StartTransfer(uint8 pageIndex);
			void Clock(uint32 clockCount);
			bool IsTransferring();

			void Reset();

		private:
			Bus& bus;
			uint8 pageIndex = 0, pageAddress = 0, dataBuffer = 0;
			bool isTransferring = false, shouldWaitForClockSync = true;
		};

		CPU cpu;
		PPU ppu;
		APU apu;
		uint8 ram[2048];
		uint32 clockCount = 0;
		Controller controllers[2];
		DirectMemoryAccessor dma;

		std::shared_ptr<Cartridge> cartridge;
	};
}
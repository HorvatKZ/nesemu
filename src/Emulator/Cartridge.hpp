#pragma once

#include "defines.h"

#include <string>
#include <vector>
#include <memory>

#include "Mapper.hpp"

#ifdef LOG_MAPPER
	#include "Logger.hpp"
#endif


namespace nes {

	class Cartridge
	{
	public:
		Cartridge(const std::string& fileName);

		uint8 CPURead(uint16 address) const;
		void CPUWrite(uint16 address, uint8 value);

		uint8 PPURead(uint16 address) const;
		void PPUWrite(uint16 address, uint8 value);

		MirroringMode GetMirroring() const;

		void Reset();

	private:
		struct Header {
			char name[4];
			uint8 programROMChunks;
			uint8 characterROMChunks;
			uint8 mapper1;
			uint8 mapper2;
			uint8 programRAMChunks;
			uint8 tvSystem1;
			uint8 tvSystem2;
			char unused[5];
		};

		struct MapperReadResultVisitor {
			const std::vector<uint8>& memory;

			MapperReadResultVisitor(const std::vector<uint8>& memory);

			uint8 operator()(const Mapper::MappedAddress& mappedAddress) const;
			uint8 operator()(const Mapper::DataFromRAM& dataFromRAM) const;
			uint8 operator()(const Mapper::EmptyResult&) const;
		};

		struct MapperWriteResultVisitor {
			std::vector<uint8>& memory;
			uint8 value = 0;

			MapperWriteResultVisitor(std::vector<uint8>& memory);

			void operator()(const Mapper::MappedAddress& mappedAddress) const;
			void operator()(const Mapper::DataFromRAM& dataFromRAM) const;
			void operator()(const Mapper::EmptyResult&) const;
		};

#ifdef LOG_MAPPER
		struct MapperLoggerVisitor {
			enum Operation {
				CPURead,
				CPUWrite,
				PPURead,
				PPUWrite
			};

			Logger logger;
			Operation operation;
			uint16 address;

			MapperLoggerVisitor();

			const char* OperationToStr() const;
			void operator()(const Mapper::MappedAddress& mappedAddress);
			void operator()(const Mapper::DataFromRAM& dataFromRAM);
			void operator()(const Mapper::EmptyResult&);
		};
		mutable MapperLoggerVisitor loggerVisitor;

		void LogMapper(MapperLoggerVisitor::Operation operation, uint16 address, const Mapper::Result& result) const;
#endif

		uint8 programROMChunks;
		uint8 characterROMChunks;
		uint8 mapperID;
		MirroringMode hardwareMirroring;

		MapperReadResultVisitor cpuReadVisitor;
		MapperWriteResultVisitor cpuWriteVisitor;
		MapperReadResultVisitor ppuReadVisitor;
		MapperWriteResultVisitor ppuWriteVisitor;

		std::unique_ptr<Mapper> mapper;
		std::vector<uint8> programMemory;
		std::vector<uint8> characterMemory;

		void Load(const std::string& fileName);
		Mapper* CreateMapper();
	};
}
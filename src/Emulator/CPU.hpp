#pragma once

#include "defines.h"

#include <vector>
#include <string>

#ifdef LOG_CPU
	#include "Logger.hpp"
#endif


namespace nes {

	class Bus;

	class CPU
	{
		friend class DebugView;

	public:
		CPU(Bus& bus);

		void Clock();
		void Reset();
		void Interrupt();
		void NonMaskableInterrupt();

		std::vector<std::string> Disassemble(uint16 fromAddress, uint16 toAddress) const;

		void SetProgramCounterForTest(uint16 programCounter);

	private: // Types
		enum StatusFlag
		{
			CarryBit = (1 << 0),
			ZeroResult = (1 << 1),
			DisableInterrupt = (1 << 2),
			DecimalMode = (1 << 3),
			Break = (1 << 4),
			Unused = (1 << 5),
			Overflow = (1 << 6),
			NegativeResult = (1 << 7)
		};

		enum AddressingMode {
			IMP, IMM, ZP0, ZPX, ZPY, REL, ABS, ABX, ABY, IND, IZX, IZY
		};

		enum Operation {
			ADC, AND, ASL, BCC, BCS, BEQ, BIT, BMI, BNE, BPL, BRK, BVC, BVS, CLC,
			CLD, CLI, CLV, CMP, CPX, CPY, DEC, DEX, DEY, EOR, INC, INX, INY, JMP,
			JSR, LDA, LDX, LDY, LSR, NOP, ORA, PHA, PHP, PLA, PLP, ROL, ROR, RTI,
			RTS, SBC, SEC, SED, SEI, STA, STX, STY, TAX, TAY, TSX, TXA, TXS, TYA,
			XXX
		};

		struct AddressingData {
			uint16 absoluteAddress = 0;
			int8 relativeAddress = 0;
			bool couldNeedExtraCycle = false;
			AddressingMode mode;
		};

		class AddressingExecutor {
		public:
			AddressingExecutor(CPU& cpu);

			AddressingData Implied();
			AddressingData Immediate();
			AddressingData ZeroPageNoOffset();
			AddressingData ZeroPageXOffset();
			AddressingData ZeroPageYOffset();
			AddressingData Relative();
			AddressingData AbsoluteNoOffset();
			AddressingData AbsoluteXOffset();
			AddressingData AbsoluteYOffset();
			AddressingData Indirect();
			AddressingData IndirectX();
			AddressingData IndirectY();

		private:
			CPU& cpu;

			AddressingData ZeroPage(uint8 offset);
			AddressingData Absolute(uint8 offset);
		};

		class OperationExecutor {
		public:
			OperationExecutor(CPU& cpu);

			bool AddWithCarry(const AddressingData& data);
			bool SubstractWithBorrow(const AddressingData& data);
			bool And(const AddressingData& data);
			bool Or(const AddressingData& data);
			bool Xor(const AddressingData& data);
			bool ShiftLeft(const AddressingData& data);
			bool ShiftRight(const AddressingData& data);
			bool RotateLeft(const AddressingData& data);
			bool RotateRight(const AddressingData& data);
			bool BitTest(const AddressingData& data);
			bool Jump(const AddressingData& data);
			bool JumpToSubrutine(const AddressingData& data);
			bool DoBreak(const AddressingData& data);
			bool PushAccumulatorToStack(const AddressingData& data);
			bool PushStatusToStack(const AddressingData& data);
			bool PopAccumulatorFromStack(const AddressingData& data);
			bool PopStatusFromStack(const AddressingData& data);
			bool ReturnFromInterrupt(const AddressingData& data);
			bool ReturnFromSubrutine(const AddressingData& data);

			template <StatusFlag flag, bool value> bool SetStatusFlag(const AddressingData& data);
			template <StatusFlag flag, bool value> bool BranchIfFlagSet(const AddressingData& data);
			template <uint8 CPU::* member> bool Compare(const AddressingData& data);
			template <int8 diff> bool DecIncMemory(const AddressingData& data);
			template <uint8 CPU::* member, int8 diff> bool DecIncMember(const AddressingData& data);
			template <uint8 CPU::* member> bool LoadMember(const AddressingData& data);
			template <uint8 CPU::* member> bool StoreMember(const AddressingData& data);
			template <uint8 CPU::* from, uint8 CPU::* to> bool TransferMembers(const AddressingData& data);

			bool NoOperation(const AddressingData& data);
			bool IllegalOperation(const AddressingData& data);

		private:
			CPU& cpu;

			bool AddSub(uint8 value);
			bool BitwiseLogicOperation(const AddressingData& data, uint8(*operation)(uint8, uint8));
			bool BitwiseShiftOperation(const AddressingData& data, uint8(*operation)(uint8, bool), bool(*carry)(uint8));
			void SetCZN(uint16 result);
			void SetZN(uint8 result);
		};

		typedef AddressingData(CPU::AddressingExecutor::* AddressingFunc)();
		typedef bool (CPU::OperationExecutor::* OperationFunc)(const AddressingData&);

		struct Instruction {
			AddressingMode addressing;
			Operation operation;
			uint8 cycles;
			std::string addressingName;
			std::string operationName;
		};

	private: // Members
		Bus& bus;

		uint8 accumulator = 0, registerX = 0, registerY = 0;
		uint8 stackPointer = 0, status = 0;
		uint16 programCounter = 0;
		uint8 occupiedFor = 0;
		uint32 clockCount = 0;
		bool justFinishedAnInstruction = false;

		AddressingExecutor addressingExecutor;
		OperationExecutor operationExecutor;
		std::vector<AddressingFunc> addressingFuncTable;
		std::vector<OperationFunc> operationFuncTable;
		std::vector<Instruction> instructionTable;

#ifdef LOG_CPU
		Logger logger;
#endif

		static const uint16 stackBase;

	private: // Methods
		uint8 Read(uint16 address) const;
		uint8 ReadFromStack();
		uint8 ReadProgramCounter();
		uint16 ReadProgramCounterLoHi();

		uint8 Fetch(const AddressingData& data) const;

		void Write(uint16 address, uint8 value);
		void WriteToStack(uint8 value);
		void WriteToStack(uint16 value);

		bool GetFlag(StatusFlag flag) const;
		void SetFlag(StatusFlag flag, bool value);

		void FillAddressingFuncTable();
		void FillOperationFuncTable();
		void FillInstructionTable();
	};
}
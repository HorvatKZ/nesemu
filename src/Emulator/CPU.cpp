#include "CPU.hpp"
#include "CPU.hpp"
#include "CPU.hpp"
#include "Bus.hpp"


namespace nes {

	static inline uint16 CombineUint8s(uint8 hi, uint8 lo)
	{
		return ((uint16)hi << 8) | lo;
	}


	static inline uint8 ZeroPageAdd(uint8 a, uint8 b)
	{
		return ((uint16)a + (uint16)b) & 0x00FF;
	}


	static std::string ToFixedLengthHex(uint16 value, uint8 length)
	{
		std::string result(length, '0');
		for (int16 i = length - 1; i >= 0; i--, value >>= 4)
			result[i] = "0123456789ABCDEF"[value & 0xF];
		return result;
	};
	
	static std::string ToHex(uint16 value, char prefix = '$')
	{
		return prefix + ToFixedLengthHex(value, 4);
	};
	
	static std::string ToHex(uint8 value, char prefix = '$')
	{
		return prefix + ToFixedLengthHex(value, 2);
	};


	const uint16 CPU::stackBase = 0x0100;


	CPU::CPU(Bus& bus) : bus(bus),
#ifdef LOG_CPU
		logger("cpudump.txt"),
#endif
		addressingExecutor(*this), addressingFuncTable(12),
		operationExecutor(*this), operationFuncTable(57)
	{
		FillAddressingFuncTable();
		FillOperationFuncTable();
		FillInstructionTable();
	}

	void CPU::Clock()
	{
		justFinishedAnInstruction = false;

		if (occupiedFor == 0) {
#ifdef LOG_CPU
			const uint16 programCounterToLog = programCounter;
#endif
			uint8 opCode = ReadProgramCounter();
			SetFlag(Unused, true);

			const Instruction& instruction = instructionTable[opCode];
			occupiedFor = instruction.cycles;

			AddressingData data = (addressingExecutor.*addressingFuncTable[instruction.addressing])();
			data.mode = instruction.addressing;
			const bool operationCouldNeedExtraCycle = (operationExecutor.*operationFuncTable[instruction.operation])(data);

			if (data.couldNeedExtraCycle && operationCouldNeedExtraCycle) {
				++occupiedFor;
			}

			SetFlag(Unused, true);

#ifdef LOG_CPU
		logger.Log("PC:%04X A:%02X X:%02X Y:%02X STKP:%02X %c%c%c%c%c%c%c%c",
			/*clockCount,*/ programCounterToLog, accumulator, registerX, registerY, stackPointer,
			GetFlag(CPU::NegativeResult) ? 'N' : '.',
			GetFlag(CPU::Overflow) ? 'V' : '.',
			GetFlag(CPU::Unused) ? 'U' : '.',
			GetFlag(CPU::Break) ? 'B' : '.',
			GetFlag(CPU::DecimalMode) ? 'D' : '.',
			GetFlag(CPU::DisableInterrupt) ? 'I' : '.',
			GetFlag(CPU::ZeroResult) ? 'Z' : '.',
			GetFlag(CPU::CarryBit) ? 'C' : '.'
		);
#endif
			justFinishedAnInstruction = true;
		}
		
		--occupiedFor;
		++clockCount;
	}


	void CPU::Reset()
	{
		programCounter = CombineUint8s(Read(0xFFFD), Read(0xFFFC));
		accumulator = 0;
		registerX = 0;
		registerY = 0;
		stackPointer = 0xFD;
		status = Unused;

		occupiedFor = 8;
		clockCount = 0;
	}


	void CPU::Interrupt()
	{
		if (!GetFlag(DisableInterrupt)) {
			NonMaskableInterrupt();
		}
	}


	void CPU::NonMaskableInterrupt()
	{
		WriteToStack(programCounter);

		SetFlag(Break, false);
		SetFlag(Unused, true);
		SetFlag(DisableInterrupt, true);
		WriteToStack(status);

		programCounter = CombineUint8s(Read(0xFFFB), Read(0xFFFA));

		occupiedFor = 7;
	}

	std::vector<std::string> CPU::Disassemble(uint16 fromAddress, uint16 toAddress) const
	{
		std::vector<std::string> dissassembly;
		uint32 currAddress = fromAddress;

		while (currAddress < toAddress) {
			uint16 entryAddress = currAddress;
			uint8 opCode = Read(currAddress++);
			const Instruction& instruction = instructionTable[opCode];

			std::string params = "";
			uint8 hi, lo, offset;
			switch (instruction.addressing) {
				case IMP:
					break;
				case IMM:
					params = ToHex(Read(currAddress++), '#') + ' ';
					break;
				case ZP0: case ZPX: case ZPY: case IZX: case IZY:
					params = ToHex(Read(currAddress++)) + ' ';
					break;
				case ABS: case ABX: case ABY: case IND:
					lo = Read(currAddress++);
					hi = Read(currAddress++);
					params = ToHex(CombineUint8s(hi, lo)) + ' ';
					break;
				case REL:
					offset = Read(currAddress++);
					params = ToHex(offset) + " (" + ToHex(static_cast<uint16>(currAddress + (int8)offset)) + ") ";
					break;
			}

			dissassembly.push_back(ToHex(entryAddress) + ": " + instruction.operationName + " " + params + "{" + instruction.addressingName + "}");
		}

		return dissassembly;
	}

	void CPU::SetProgramCounterForTest(uint16 programCounter)
	{
		this->programCounter = programCounter;
	}


	uint8 CPU::Read(uint16 address) const
	{
		return bus.Read(address);
	}


	void CPU::Write(uint16 address, uint8 value)
	{
		bus.Write(address, value);
	}


	void CPU::WriteToStack(uint8 value)
	{
		Write(stackBase + stackPointer, value);
		stackPointer--;
	}


	void CPU::WriteToStack(uint16 value)
	{
		uint8 pcHi = value >> 8;
		uint8 pcLo = (uint8)value;
		WriteToStack(pcHi);
		WriteToStack(pcLo);
	}

	uint8 CPU::ReadFromStack()
	{
		stackPointer++;
		return Read(stackBase + stackPointer);
	}


	uint8 CPU::ReadProgramCounter()
	{
		return Read(programCounter++);
	}


	uint16 CPU::ReadProgramCounterLoHi()
	{
		const uint8 lo = ReadProgramCounter();
		const uint8 hi = ReadProgramCounter();
		return CombineUint8s(hi, lo);
	}


	bool CPU::GetFlag(StatusFlag flag) const
	{
		return status & flag;
	}


	void CPU::SetFlag(StatusFlag flag, bool value)
	{
		if (value) {
			status |= flag;
		} else {
			status &= ~flag;
		}
	}

	uint8 CPU::Fetch(const AddressingData& data) const
	{
		return data.mode == IMP ? accumulator : Read(data.absoluteAddress);
	}


	// === AddressingExecutor ===

	CPU::AddressingExecutor::AddressingExecutor(CPU& cpu) : cpu(cpu)
	{
	}

	CPU::AddressingData CPU::AddressingExecutor::Implied()
	{
		return { 0, 0, false }; // Might need to fetch the accumulator
	}

	CPU::AddressingData CPU::AddressingExecutor::Immediate()
	{
		return { cpu.programCounter++, 0, false };
	}

	CPU::AddressingData CPU::AddressingExecutor::ZeroPageNoOffset()
	{
		return ZeroPage(0);
	}

	CPU::AddressingData CPU::AddressingExecutor::ZeroPageXOffset()
	{
		return ZeroPage(cpu.registerX);
	}

	CPU::AddressingData CPU::AddressingExecutor::ZeroPageYOffset()
	{
		return ZeroPage(cpu.registerY);
	}

	CPU::AddressingData CPU::AddressingExecutor::ZeroPage(uint8 offset)
	{
		return { ZeroPageAdd(cpu.ReadProgramCounter(), offset), 0, false};
	}

	CPU::AddressingData CPU::AddressingExecutor::Relative()
	{
		return { 0, static_cast<int8>(cpu.ReadProgramCounter()), false };
	}

	CPU::AddressingData CPU::AddressingExecutor::AbsoluteNoOffset()
	{
		return Absolute(0);
	}

	CPU::AddressingData CPU::AddressingExecutor::AbsoluteXOffset()
	{
		return Absolute(cpu.registerX);
	}

	CPU::AddressingData CPU::AddressingExecutor::AbsoluteYOffset()
	{
		return Absolute(cpu.registerY);
	}

	CPU::AddressingData CPU::AddressingExecutor::Absolute(uint8 offset)
	{
		uint16 readAddress = cpu.ReadProgramCounterLoHi();
		uint16 absoluteAddress = readAddress + offset;
		const bool pageCrossed = (readAddress & 0xFF00) != (absoluteAddress & 0xFF00);
		return { absoluteAddress, 0, pageCrossed };
	}

	CPU::AddressingData CPU::AddressingExecutor::Indirect()
	{
		uint16 pointer = cpu.ReadProgramCounterLoHi();
		const bool isPointerAtPageBoundary = (pointer & 0x00FF) == 0x00FF;
		if (isPointerAtPageBoundary) {
			return { CombineUint8s(cpu.Read(pointer & 0xFF00), cpu.Read(pointer)), 0, false };
		} else {
			return { CombineUint8s(cpu.Read(pointer + 1), cpu.Read(pointer)), 0, false };
		}
	}

	CPU::AddressingData CPU::AddressingExecutor::IndirectX()
	{
		uint8 offset = cpu.ReadProgramCounter();
		uint8 offsettedX = ZeroPageAdd(offset, cpu.registerX);
		uint8 lo = cpu.Read(offsettedX);
		uint8 hi = cpu.Read(ZeroPageAdd(offsettedX, 1));
		return { CombineUint8s(hi, lo), 0, false };
	}

	CPU::AddressingData CPU::AddressingExecutor::IndirectY()
	{
		uint8 pointerOnPageZero = cpu.ReadProgramCounter();
		uint16 readAddress = CombineUint8s(cpu.Read(ZeroPageAdd(pointerOnPageZero, 1)), cpu.Read(pointerOnPageZero));
		uint16 absoluteAddress = readAddress + cpu.registerY;
		const bool pageCrossed = (readAddress & 0xFF00) != (absoluteAddress & 0xFF);
		return { absoluteAddress, 0, pageCrossed };
	}


	// === OperationExecutor ===

	CPU::OperationExecutor::OperationExecutor(CPU& cpu) : cpu(cpu)
	{
	}

	bool CPU::OperationExecutor::AddWithCarry(const AddressingData& data)
	{
		uint8 fetched = cpu.Fetch(data);
		return AddSub(fetched);
	}

	bool CPU::OperationExecutor::SubstractWithBorrow(const AddressingData& data)
	{
		uint8 fetched = cpu.Fetch(data);
		uint8 invertedData = ~fetched;
		return AddSub(invertedData);
	}

	bool CPU::OperationExecutor::AddSub(uint8 value)
	{
		uint16 result = (uint16)cpu.accumulator + (uint16)value + (uint16)cpu.GetFlag(CarryBit);

		SetCZN(result);
		cpu.SetFlag(Overflow, (~((uint16)cpu.accumulator ^ (uint16)value) & ((uint16)cpu.accumulator ^ result)) & 0x0080);

		cpu.accumulator = (uint8)result;
		return true;
	}

	bool CPU::OperationExecutor::And(const AddressingData& data)
	{
		return BitwiseLogicOperation(data, [](uint8 a, uint8 b) -> uint8 { return a & b; });
	}

	bool CPU::OperationExecutor::Or(const AddressingData& data)
	{
		return BitwiseLogicOperation(data, [](uint8 a, uint8 b) -> uint8 { return a | b; });
	}

	bool CPU::OperationExecutor::Xor(const AddressingData& data)
	{
		return BitwiseLogicOperation(data, [](uint8 a, uint8 b) -> uint8 { return a ^ b; });
	}

	bool CPU::OperationExecutor::BitwiseLogicOperation(const AddressingData& data, uint8(*operation)(uint8, uint8))
	{
		uint8 fetched = cpu.Fetch(data);
		cpu.accumulator = operation(cpu.accumulator, fetched);
		SetZN(cpu.accumulator);
		return true;
	}

	bool CPU::OperationExecutor::ShiftLeft(const AddressingData& data)
	{
		return BitwiseShiftOperation(data,
			[](uint8 a, bool carry) -> uint8 { return a << 1; },
			[](uint8 a) -> bool { return a & 0x80; });
	}

	bool CPU::OperationExecutor::ShiftRight(const AddressingData& data)
	{
		return BitwiseShiftOperation(data,
			[](uint8 a, bool carry) -> uint8 { return a >> 1; },
			[](uint8 a) -> bool { return a & 0x01; });
	}

	bool CPU::OperationExecutor::RotateLeft(const AddressingData& data)
	{
		return BitwiseShiftOperation(data,
			[](uint8 a, bool carry) -> uint8 { return (a << 1) | (uint8)carry; },
			[](uint8 a) -> bool { return a & 0x80; });
	}

	bool CPU::OperationExecutor::RotateRight(const AddressingData& data)
	{
		return BitwiseShiftOperation(data,
			[](uint8 a, bool carry) -> uint8 { return (a >> 1) | ((uint8)carry << 7); },
			[](uint8 a) -> bool { return a & 0x01; });
	}

	bool CPU::OperationExecutor::BitwiseShiftOperation(const AddressingData& data, uint8(*operation)(uint8, bool), bool(*carry)(uint8))
	{
		uint8 fetched = cpu.Fetch(data);
		uint8 shifted = operation(fetched, cpu.GetFlag(CarryBit));
		SetZN(shifted);
		cpu.SetFlag(CarryBit, carry(fetched));

		if (data.mode == IMP) {
			cpu.accumulator = shifted;
		} else {
			cpu.Write(data.absoluteAddress, shifted);
		}
		return false;
	}

	bool CPU::OperationExecutor::BitTest(const AddressingData& data)
	{
		uint8 fetched = cpu.Fetch(data);
		cpu.SetFlag(ZeroResult, (cpu.accumulator & fetched) == 0);
		cpu.SetFlag(NegativeResult, fetched & (1 << 7));
		cpu.SetFlag(Overflow, fetched & (1 << 6));
		return false;
	}

	bool CPU::OperationExecutor::Jump(const AddressingData& data)
	{
		cpu.programCounter = data.absoluteAddress;
		return false;
	}

	bool CPU::OperationExecutor::JumpToSubrutine(const AddressingData& data)
	{
		cpu.WriteToStack(static_cast<uint16>(cpu.programCounter - 1));
		cpu.programCounter = data.absoluteAddress;
		return false;
	}

	bool CPU::OperationExecutor::DoBreak(const AddressingData& data)
	{
		cpu.WriteToStack(static_cast<uint16>(cpu.programCounter + 1));

		cpu.SetFlag(DisableInterrupt, true);
		cpu.SetFlag(Break, true);
		cpu.WriteToStack(cpu.status);
		cpu.SetFlag(Break, false);

		cpu.programCounter = CombineUint8s(cpu.Read(0xFFFF), cpu.Read(0xFFFE));
		return false;
	}

	bool CPU::OperationExecutor::PushAccumulatorToStack(const AddressingData& data)
	{
		cpu.WriteToStack(cpu.accumulator);
		return false;
	}

	bool CPU::OperationExecutor::PushStatusToStack(const AddressingData& data)
	{
		cpu.SetFlag(Break, true);
		cpu.SetFlag(Unused, true);
		cpu.WriteToStack(cpu.status);
		cpu.SetFlag(Break, false);
		cpu.SetFlag(Unused, false);
		return false;
	}

	bool CPU::OperationExecutor::PopAccumulatorFromStack(const AddressingData& data)
	{
		cpu.accumulator = cpu.ReadFromStack();
		SetZN(cpu.accumulator);
		return false;
	}

	bool CPU::OperationExecutor::PopStatusFromStack(const AddressingData& data)
	{
		cpu.status = cpu.ReadFromStack();
		cpu.SetFlag(Unused, true);
		return false;
	}

	bool CPU::OperationExecutor::ReturnFromInterrupt(const AddressingData& data)
	{
		cpu.status = cpu.ReadFromStack();
		cpu.SetFlag(Break, false);
		cpu.SetFlag(Unused, false);

		uint8 lo = cpu.ReadFromStack();
		uint8 hi = cpu.ReadFromStack();
		cpu.programCounter = CombineUint8s(hi, lo);
		return false;
	}

	bool CPU::OperationExecutor::ReturnFromSubrutine(const AddressingData& data)
	{
		uint8 lo = cpu.ReadFromStack();
		uint8 hi = cpu.ReadFromStack();
		cpu.programCounter = CombineUint8s(hi, lo) + 1;
		return false;
	}

	template <CPU::StatusFlag flag, bool value>
	bool CPU::OperationExecutor::SetStatusFlag(const AddressingData& data)
	{
		cpu.SetFlag(flag, value);
		return false;
	}

	template<CPU::StatusFlag flag, bool value>
	bool CPU::OperationExecutor::BranchIfFlagSet(const AddressingData& data)
	{
		if (cpu.GetFlag(flag) == value) {
			uint16 prevProgramCounter = cpu.programCounter;
			cpu.programCounter += data.relativeAddress;
			const bool pageCrossed = (prevProgramCounter & 0xFF00) != (cpu.programCounter & 0xFF00);
			cpu.occupiedFor += pageCrossed ? 2 : 1;
		}
		return false;
	}

	template<uint8 CPU::*member>
	bool CPU::OperationExecutor::Compare(const AddressingData& data)
	{
		uint8 fetched = cpu.Fetch(data);
		uint16 diff = (uint16)(cpu.*member) - (uint16)fetched;
		SetZN(diff);
		cpu.SetFlag(CarryBit, cpu.*member >= fetched);
		return member == &CPU::accumulator;
	}

	template<int8 diff>
	bool CPU::OperationExecutor::DecIncMemory(const AddressingData& data)
	{
		uint8 fetched = cpu.Fetch(data);
		fetched += diff;
		cpu.Write(data.absoluteAddress, fetched);
		SetZN(fetched);
		return false;
	}

	template<uint8 CPU::*member, int8 diff>
	bool CPU::OperationExecutor::DecIncMember(const AddressingData& data)
	{
		cpu.*member += diff;
		SetZN(cpu.*member);
		return false;
	}

	template<uint8 CPU::*member>
	bool CPU::OperationExecutor::LoadMember(const AddressingData& data)
	{
		uint8 fetched = cpu.Fetch(data);
		cpu.*member = fetched;
		SetZN(fetched);
		return true;
	}

	template<uint8 CPU::*member>
	bool CPU::OperationExecutor::StoreMember(const AddressingData& data)
	{
		cpu.Write(data.absoluteAddress, cpu.*member);
		return false;
	}

	template<uint8 CPU::*from, uint8 CPU::*to>
	bool CPU::OperationExecutor::TransferMembers(const AddressingData& data)
	{
		cpu.*to = cpu.*from;
		if (to != &CPU::stackPointer) {
			SetZN(cpu.*to);
		}
		return false;
	}

	void CPU::OperationExecutor::SetCZN(uint16 result)
	{
		cpu.SetFlag(CarryBit, result & 0xFF00);
		cpu.SetFlag(ZeroResult, (result & 0x00FF) == 0);
		cpu.SetFlag(NegativeResult, result & 0x0080);
	}

	void CPU::OperationExecutor::SetZN(uint8 result)
	{
		cpu.SetFlag(ZeroResult, result == 0);
		cpu.SetFlag(NegativeResult, result & 0x80);
	}

	bool CPU::OperationExecutor::NoOperation(const AddressingData& data)
	{
		return true;
	}

	bool CPU::OperationExecutor::IllegalOperation(const AddressingData& data)
	{
		return false;
	}


	// CPU Fillers

	void CPU::FillAddressingFuncTable()
	{
		addressingFuncTable[IMP] = &AddressingExecutor::Implied;
		addressingFuncTable[IMM] = &AddressingExecutor::Immediate;
		addressingFuncTable[ZP0] = &AddressingExecutor::ZeroPageNoOffset;
		addressingFuncTable[ZPX] = &AddressingExecutor::ZeroPageXOffset;
		addressingFuncTable[ZPY] = &AddressingExecutor::ZeroPageYOffset;
		addressingFuncTable[REL] = &AddressingExecutor::Relative;
		addressingFuncTable[ABS] = &AddressingExecutor::AbsoluteNoOffset;
		addressingFuncTable[ABX] = &AddressingExecutor::AbsoluteXOffset;
		addressingFuncTable[ABY] = &AddressingExecutor::AbsoluteYOffset;
		addressingFuncTable[IND] = &AddressingExecutor::Indirect;
		addressingFuncTable[IZX] = &AddressingExecutor::IndirectX;
		addressingFuncTable[IZY] = &AddressingExecutor::IndirectY;
	}


	void CPU::FillOperationFuncTable()
	{
		operationFuncTable[ADC] = &OperationExecutor::AddWithCarry;
		operationFuncTable[AND] = &OperationExecutor::And;
		operationFuncTable[ASL] = &OperationExecutor::ShiftLeft;
		operationFuncTable[BCC] = &OperationExecutor::BranchIfFlagSet<CarryBit, false>;
		operationFuncTable[BCS] = &OperationExecutor::BranchIfFlagSet<CarryBit, true>;
		operationFuncTable[BEQ] = &OperationExecutor::BranchIfFlagSet<ZeroResult, true>;
		operationFuncTable[BIT] = &OperationExecutor::BitTest;
		operationFuncTable[BMI] = &OperationExecutor::BranchIfFlagSet<NegativeResult, true>;
		operationFuncTable[BNE] = &OperationExecutor::BranchIfFlagSet<ZeroResult, false>;
		operationFuncTable[BPL] = &OperationExecutor::BranchIfFlagSet<NegativeResult, false>;
		operationFuncTable[BRK] = &OperationExecutor::DoBreak;
		operationFuncTable[BVC] = &OperationExecutor::BranchIfFlagSet<Overflow, false>;
		operationFuncTable[BVS] = &OperationExecutor::BranchIfFlagSet<Overflow, true>;
		operationFuncTable[CLC] = &OperationExecutor::SetStatusFlag<CarryBit, false>;
		operationFuncTable[CLD] = &OperationExecutor::SetStatusFlag<DecimalMode, false>;
		operationFuncTable[CLI] = &OperationExecutor::SetStatusFlag<DisableInterrupt, false>;
		operationFuncTable[CLV] = &OperationExecutor::SetStatusFlag<Overflow, false>;
		operationFuncTable[CMP] = &OperationExecutor::Compare<&CPU::accumulator>;
		operationFuncTable[CPX] = &OperationExecutor::Compare<&CPU::registerX>;
		operationFuncTable[CPY] = &OperationExecutor::Compare<&CPU::registerY>;
		operationFuncTable[DEC] = &OperationExecutor::DecIncMemory<-1>;
		operationFuncTable[DEX] = &OperationExecutor::DecIncMember<&CPU::registerX, -1>;
		operationFuncTable[DEY] = &OperationExecutor::DecIncMember<&CPU::registerY, -1>;
		operationFuncTable[EOR] = &OperationExecutor::Xor;
		operationFuncTable[INC] = &OperationExecutor::DecIncMemory<1>;
		operationFuncTable[INX] = &OperationExecutor::DecIncMember<&CPU::registerX, 1>;
		operationFuncTable[INY] = &OperationExecutor::DecIncMember<&CPU::registerY, 1>;
		operationFuncTable[JMP] = &OperationExecutor::Jump;
		operationFuncTable[JSR] = &OperationExecutor::JumpToSubrutine;
		operationFuncTable[LDA] = &OperationExecutor::LoadMember<&CPU::accumulator>;
		operationFuncTable[LDX] = &OperationExecutor::LoadMember<&CPU::registerX>;
		operationFuncTable[LDY] = &OperationExecutor::LoadMember<&CPU::registerY>;
		operationFuncTable[LSR] = &OperationExecutor::ShiftRight;
		operationFuncTable[NOP] = &OperationExecutor::NoOperation;
		operationFuncTable[ORA] = &OperationExecutor::Or;
		operationFuncTable[PHA] = &OperationExecutor::PushAccumulatorToStack;
		operationFuncTable[PHP] = &OperationExecutor::PushStatusToStack;
		operationFuncTable[PLA] = &OperationExecutor::PopAccumulatorFromStack;
		operationFuncTable[PLP] = &OperationExecutor::PopStatusFromStack;
		operationFuncTable[ROL] = &OperationExecutor::RotateLeft;
		operationFuncTable[ROR] = &OperationExecutor::RotateRight;
		operationFuncTable[RTI] = &OperationExecutor::ReturnFromInterrupt;
		operationFuncTable[RTS] = &OperationExecutor::ReturnFromSubrutine;
		operationFuncTable[SBC] = &OperationExecutor::SubstractWithBorrow;
		operationFuncTable[SEC] = &OperationExecutor::SetStatusFlag<CarryBit, true>;
		operationFuncTable[SED] = &OperationExecutor::SetStatusFlag<DecimalMode, true>;
		operationFuncTable[SEI] = &OperationExecutor::SetStatusFlag<DisableInterrupt, true>;
		operationFuncTable[STA] = &OperationExecutor::StoreMember<&CPU::accumulator>;
		operationFuncTable[STX] = &OperationExecutor::StoreMember<&CPU::registerX>;
		operationFuncTable[STY] = &OperationExecutor::StoreMember<&CPU::registerY>;
		operationFuncTable[TAX] = &OperationExecutor::TransferMembers<&CPU::accumulator, &CPU::registerX>;
		operationFuncTable[TAY] = &OperationExecutor::TransferMembers<&CPU::accumulator, &CPU::registerY>;
		operationFuncTable[TSX] = &OperationExecutor::TransferMembers<&CPU::stackPointer, &CPU::registerX>;
		operationFuncTable[TXA] = &OperationExecutor::TransferMembers<&CPU::registerX, &CPU::accumulator>;
		operationFuncTable[TXS] = &OperationExecutor::TransferMembers<&CPU::registerX, &CPU::stackPointer>;
		operationFuncTable[TYA] = &OperationExecutor::TransferMembers<&CPU::registerY, &CPU::accumulator>;
		operationFuncTable[XXX] = &OperationExecutor::IllegalOperation;
	}


#define OP(operation, addressing, cycles) { addressing, operation, cycles, #addressing, #operation }
	void CPU::FillInstructionTable()
	{
		instructionTable = {
			OP(BRK, IMM, 7), OP(ORA, IZX, 6), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(NOP, IMP, 3), OP(ORA, ZP0, 3), OP(ASL, ZP0, 5), OP(XXX, IMP, 5), OP(PHP, IMP, 3), OP(ORA, IMM, 2), OP(ASL, IMP, 2), OP(XXX, IMP, 2), OP(NOP, IMP, 4), OP(ORA, ABS, 4), OP(ASL, ABS, 6), OP(XXX, IMP, 6),
			OP(BPL, REL, 2), OP(ORA, IZY, 5), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(NOP, IMP, 4), OP(ORA, ZPX, 4), OP(ASL, ZPX, 6), OP(XXX, IMP, 6), OP(CLC, IMP, 2), OP(ORA, ABY, 4), OP(NOP, IMP, 2), OP(XXX, IMP, 7), OP(NOP, IMP, 4), OP(ORA, ABX, 4), OP(ASL, ABX, 7), OP(XXX, IMP, 7),
			OP(JSR, ABS, 6), OP(AND, IZX, 6), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(BIT, ZP0, 3), OP(AND, ZP0, 3), OP(ROL, ZP0, 5), OP(XXX, IMP, 5), OP(PLP, IMP, 4), OP(AND, IMM, 2), OP(ROL, IMP, 2), OP(XXX, IMP, 2), OP(BIT, ABS, 4), OP(AND, ABS, 4), OP(ROL, ABS, 6), OP(XXX, IMP, 6),
			OP(BMI, REL, 2), OP(AND, IZY, 5), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(NOP, IMP, 4), OP(AND, ZPX, 4), OP(ROL, ZPX, 6), OP(XXX, IMP, 6), OP(SEC, IMP, 2), OP(AND, ABY, 4), OP(NOP, IMP, 2), OP(XXX, IMP, 7), OP(NOP, IMP, 4), OP(AND, ABX, 4), OP(ROL, ABX, 7), OP(XXX, IMP, 7),
			OP(RTI, IMP, 6), OP(EOR, IZX, 6), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(NOP, IMP, 3), OP(EOR, ZP0, 3), OP(LSR, ZP0, 5), OP(XXX, IMP, 5), OP(PHA, IMP, 3), OP(EOR, IMM, 2), OP(LSR, IMP, 2), OP(XXX, IMP, 2), OP(JMP, ABS, 3), OP(EOR, ABS, 4), OP(LSR, ABS, 6), OP(XXX, IMP, 6),
			OP(BVC, REL, 2), OP(EOR, IZY, 5), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(NOP, IMP, 4), OP(EOR, ZPX, 4), OP(LSR, ZPX, 6), OP(XXX, IMP, 6), OP(CLI, IMP, 2), OP(EOR, ABY, 4), OP(NOP, IMP, 2), OP(XXX, IMP, 7), OP(NOP, IMP, 4), OP(EOR, ABX, 4), OP(LSR, ABX, 7), OP(XXX, IMP, 7),
			OP(RTS, IMP, 6), OP(ADC, IZX, 6), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(NOP, IMP, 3), OP(ADC, ZP0, 3), OP(ROR, ZP0, 5), OP(XXX, IMP, 5), OP(PLA, IMP, 4), OP(ADC, IMM, 2), OP(ROR, IMP, 2), OP(XXX, IMP, 2), OP(JMP, IND, 5), OP(ADC, ABS, 4), OP(ROR, ABS, 6), OP(XXX, IMP, 6),
			OP(BVS, REL, 2), OP(ADC, IZY, 5), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(NOP, IMP, 4), OP(ADC, ZPX, 4), OP(ROR, ZPX, 6), OP(XXX, IMP, 6), OP(SEI, IMP, 2), OP(ADC, ABY, 4), OP(NOP, IMP, 2), OP(XXX, IMP, 7), OP(NOP, IMP, 4), OP(ADC, ABX, 4), OP(ROR, ABX, 7), OP(XXX, IMP, 7),
			OP(NOP, IMP, 2), OP(STA, IZX, 6), OP(NOP, IMP, 2), OP(XXX, IMP, 6), OP(STY, ZP0, 3), OP(STA, ZP0, 3), OP(STX, ZP0, 3), OP(XXX, IMP, 3), OP(DEY, IMP, 2), OP(NOP, IMP, 2), OP(TXA, IMP, 2), OP(XXX, IMP, 2), OP(STY, ABS, 4), OP(STA, ABS, 4), OP(STX, ABS, 4), OP(XXX, IMP, 4),
			OP(BCC, REL, 2), OP(STA, IZY, 6), OP(XXX, IMP, 2), OP(XXX, IMP, 6), OP(STY, ZPX, 4), OP(STA, ZPX, 4), OP(STX, ZPY, 4), OP(XXX, IMP, 4), OP(TYA, IMP, 2), OP(STA, ABY, 5), OP(TXS, IMP, 2), OP(XXX, IMP, 5), OP(NOP, IMP, 5), OP(STA, ABX, 5), OP(XXX, IMP, 5), OP(XXX, IMP, 5),
			OP(LDY, IMM, 2), OP(LDA, IZX, 6), OP(LDX, IMM, 2), OP(XXX, IMP, 6), OP(LDY, ZP0, 3), OP(LDA, ZP0, 3), OP(LDX, ZP0, 3), OP(XXX, IMP, 3), OP(TAY, IMP, 2), OP(LDA, IMM, 2), OP(TAX, IMP, 2), OP(XXX, IMP, 2), OP(LDY, ABS, 4), OP(LDA, ABS, 4), OP(LDX, ABS, 4), OP(XXX, IMP, 4),
			OP(BCS, REL, 2), OP(LDA, IZY, 5), OP(XXX, IMP, 2), OP(XXX, IMP, 5), OP(LDY, ZPX, 4), OP(LDA, ZPX, 4), OP(LDX, ZPY, 4), OP(XXX, IMP, 4), OP(CLV, IMP, 2), OP(LDA, ABY, 4), OP(TSX, IMP, 2), OP(XXX, IMP, 4), OP(LDY, ABX, 4), OP(LDA, ABX, 4), OP(LDX, ABY, 4), OP(XXX, IMP, 4),
			OP(CPY, IMM, 2), OP(CMP, IZX, 6), OP(NOP, IMP, 2), OP(XXX, IMP, 8), OP(CPY, ZP0, 3), OP(CMP, ZP0, 3), OP(DEC, ZP0, 5), OP(XXX, IMP, 5), OP(INY, IMP, 2), OP(CMP, IMM, 2), OP(DEX, IMP, 2), OP(XXX, IMP, 2), OP(CPY, ABS, 4), OP(CMP, ABS, 4), OP(DEC, ABS, 6), OP(XXX, IMP, 6),
			OP(BNE, REL, 2), OP(CMP, IZY, 5), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(NOP, IMP, 4), OP(CMP, ZPX, 4), OP(DEC, ZPX, 6), OP(XXX, IMP, 6), OP(CLD, IMP, 2), OP(CMP, ABY, 4), OP(NOP, IMP, 2), OP(XXX, IMP, 7), OP(NOP, IMP, 4), OP(CMP, ABX, 4), OP(DEC, ABX, 7), OP(XXX, IMP, 7),
			OP(CPX, IMM, 2), OP(SBC, IZX, 6), OP(NOP, IMP, 2), OP(XXX, IMP, 8), OP(CPX, ZP0, 3), OP(SBC, ZP0, 3), OP(INC, ZP0, 5), OP(XXX, IMP, 5), OP(INX, IMP, 2), OP(SBC, IMM, 2), OP(NOP, IMP, 2), OP(SBC, IMP, 2), OP(CPX, ABS, 4), OP(SBC, ABS, 4), OP(INC, ABS, 6), OP(XXX, IMP, 6),
			OP(BEQ, REL, 2), OP(SBC, IZY, 5), OP(XXX, IMP, 2), OP(XXX, IMP, 8), OP(NOP, IMP, 4), OP(SBC, ZPX, 4), OP(INC, ZPX, 6), OP(XXX, IMP, 6), OP(SED, IMP, 2), OP(SBC, ABY, 4), OP(NOP, IMP, 2), OP(XXX, IMP, 7), OP(NOP, IMP, 4), OP(SBC, ABX, 4), OP(INC, ABX, 7), OP(XXX, IMP, 7),
		};
	}

}
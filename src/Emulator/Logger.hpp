#pragma once

#include <cstdio>
#include <utility>


namespace nes {

	class Logger {
	public:
		Logger(const std::string& path)
		{
			file = fopen(path.c_str(), "wt");
		}

		~Logger()
		{
			if (file != nullptr) {
				fclose(file);
			}
		}

		template <typename... Args>
		void Log(Args&&... args) {
			if (file != nullptr) {
				fprintf(file, std::forward<Args>(args)...);
				fprintf(file, "\n");
			}
		}

	private:
		FILE* file;
	};

}
# NES Emulator

This project is my attempt to create a NES emulator using C++ and OpenGL

## Sources

- [javidx9 NES Emulator From Scratch](https://www.youtube.com/watch?v=nViZg02IMQo&list=PLrOv9FMX8xJHqMvSGB_9G9nZZ_4IgteYf&pp=iAQB)
- [Nesdev Wiki](https://www.nesdev.org/)

## Legal notice

Creating emulators and emulating the underlying hardware of a console is considered legal. However, distributing copyrighted ROMs violates the copyright law. This includes, but is not limited to downloading NES ROMs (even if you own a copy of the game in question). To legally use the emulator you need to own a copy of the game you want to play and copy it to your PC using a ROM dumper.

I do not have any intentions to commertially advertise, distribute, encourage the usage of and/or make profit on this emulator. This emulator is simply the result of a self-imposed programming challenge. I am willing to take this repository down if legal questions arise.
